﻿using System;
using System.IO;

namespace CapstoneSegway.Comm
{
    /// <summary>
    /// Provides byte stuffing using Consistent Overhead Byte Stuffing (COBS).
    /// </summary>
    /// <remarks>
    /// The length limit gives us a constant overhead of 1 byte and removes the
    /// special case for the 0xFF code. Our frames are smaller than this anyway.
    /// 
    /// For information on COBS see:
    /// http://conferences.sigcomm.org/sigcomm/1997/papers/p062.pdf
    /// 
    /// This code is based on code by Jacques Fortier.
    /// See http://www.jacquesf.com/2011/03/consistent-overhead-byte-stuffing/
    /// Copyright 2011, Jacques Fortier. All rights reserved.
    /// Redistribution and use in source and binary forms are permitted, with
    /// or without modification.
    /// </remarks>
    public class COBS
    {
        /// <summary>Byte stuffs data to remove 0x00 bytes using COBS.</summary>
        /// <param name="data">Data</param>
        /// <param name="offset">The zero-based offset into data</param>
        /// <param name="length">The number of bytes to encode (0 to 253)</param>
        /// <returns>Encoded data (length is 1 more than input)</returns>
        /// <exception cref="System.ArgumentException">Thrown when the input data is
        /// too long</exception>
        public static Byte[] Encode(Byte[] data, Int32 offset, Int32 length) {
            if (length > 253) {
                throw new ArgumentException("data must be 253 bytes or less", "data");
            }

            Byte[] output = new Byte[length + 1];
            Int32 readIndex = offset;
            Int32 writeIndex = 1;
            Int32 codeIndex = 0;
            Byte code = 1;

            while (readIndex < offset + length) {
                if (data[readIndex] == 0) {
                    // Finish the block that this zero byte ends
                    output[codeIndex] = code;

                    // Start a new block
                    code = 1;
                    codeIndex = writeIndex;
                    writeIndex++;
                    readIndex++;
                } else {
                    // Copy the non-zero byte
                    output[writeIndex++] = data[readIndex++];
                    code++;
                }
            }

            // Finish the final block
            output[codeIndex] = code;

            return output;
        }

        /// <summary>Byte stuffs data to remove 0x00 bytes using COBS.</summary>
        /// <param name="data">0 to 253 bytes of data</param>
        /// <returns>Encoded data (length is 1 more than input)</returns>
        /// <exception cref="System.ArgumentException">Thrown when the input data is
        /// too long</exception>
        public static Byte[] Encode(Byte[] data) {
            return Encode(data, 0, data.Length);
        }

        /// <summary>Decodes data encoded with COBS.</summary>
        /// <param name="encodedData">1 to 254 bytes of COBS data</param>
        /// <param name="offset">The zero-based offset into encodedData</param>
        /// <param name="length">The number of bytes to decode</param>
        /// <returns>0 to 253 bytes of decoded data</returns>
        /// <exception cref="System.ArgumentException">Thrown when the input data is
        /// too short or too long</exception>
        /// <exception cref="System.IO.InvalidDataException">Thrown when the input
        /// data contains is corrupt</exception>
        public static Byte[] Decode(Byte[] encodedData, Int32 offset, Int32 length) {
            if (length == 0 || length > 254) {
                throw new ArgumentOutOfRangeException("length", "encodedData must be 1 to 254 bytes");
            }

            Byte[] output = new Byte[length - 1];
            Int32 readIndex = offset;
            Int32 writeIndex = 0;
            Byte code;

            while (readIndex < offset + length) {
                // Start a block
                code = encodedData[readIndex];

                if (code == 0) {
                    throw new InvalidDataException("Zero byte found in COBS data");
                }
                if (readIndex + code > offset + length && code != 1) {
                    throw new InvalidDataException("Bad COBS block");
                }

                // Copy the block's data bytes
                readIndex++;
                for (Int32 i = 1; i < code; i++) { // note 1 based
                    if (encodedData[readIndex] == 0) {
                        throw new InvalidDataException("Zero byte found in COBS data");
                    }
                    output[writeIndex++] = encodedData[readIndex++];
                }

                // Finish tbe block (but suppress the implicit zero at the end)
                if (readIndex != length + offset) {
                    output[writeIndex++] = 0;
                }
            }

            return output;
        }

        /// <summary>Decodes data encoded with COBS.</summary>
        /// <param name="encodedData">1 to 254 bytes of COBS data</param>
        /// <returns>0 to 253 bytes of decoded data</returns>
        /// <exception cref="System.ArgumentException">Thrown when the input data is
        /// too short or too long</exception>
        /// <exception cref="System.IO.InvalidDataException">Thrown when the input
        /// data contains is corrupt</exception>
        public static Byte[] Decode(Byte[] encodedData) {
            return Decode(encodedData, 0, encodedData.Length);
        }
    }
}
