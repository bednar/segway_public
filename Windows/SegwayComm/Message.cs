﻿using System;
using System.IO;

namespace CapstoneSegway.Comm
{
    public enum TelemetryItem : byte
    {
        /* 0x60-0x6F -- uint16 reports */
        M1CurrentMA = 0x60,
        M2CurrentMA = 0x61,
        /* 0x70-0x7F -- int16 reports */
        M1Command = 0x70,
        M2Command = 0x71,
        PidOutput = 0x72,
        /* 0x80-0x8F -- float reports */
        AccelerometerThetaDegrees = 0x80,
        GyroThetaDotDPS = 0x81,
        EstimatedThetaDegrees = 0x82,
        EstimatedErrorDegrees = 0x83,
        ErrorIntergral = 0x84,
        BatteryVolts = 0x85
    }

    public enum ParameterItem : byte
    {
        /* 0x00-0x0F -- uint16 */
        /* 0x10-0x1F -- int16 */
        MotorDeadZone = 0x10,
        MotorRoundUp = 0x11,
        SteerAmount = 0x12,
        M1Boost = 0x13,
        M2Boost = 0x14,
        /* 0x20-0x2F -- float */
        Kp = 0x20,
        Kd = 0x21,
        Ki = 0x22,
        TargetAngle = 0x23,
        HighpassCoeffient = 0x24,
        LowpassCoeffient = 0x25,
        MaxErrorIntegral = 0x26,
        AccelOffset = 0x27
    }

    public enum OutgoingMessageClass
    {
        ParameterResponse,
        TelemetryReport,
        Reserved
    }

    public class Message
    {
        public const Byte UIntBlock = 0x00;
        public const Byte IntBlock = 0x10;
        public const Byte FloatBlock = 0x20;
        // Outgoing (from Segway)
        public const Byte ParameterResponseBase = 0x00;
        public const Byte ParameterResponseMax = 0x2F;
        public const Byte TelemetryReportBase = 0x60;
        public const Byte TelemetryReportMax = 0x8F;
        // Incoming (to Segway)
        public const Byte ParameterRequestBase = 0x00;
        public const Byte ParameterSetBase = 0x30;
        public const Byte TelemetryRequestBase = 0x60;

        private Message() { }

        public static Int32 CreateParameterValueRequest(Byte[] buffer, Int32 offset, ParameterItem item) {
            buffer[offset] = (Byte)item;
            return 1;
        }

        public static Int32 CreateParameterSetMessage(Byte[] buffer, Int32 offset, ParameterItem item, UInt16 value) {
            if (GetOutgoingMessageDataType((Byte)item) != typeof(UInt16))
                throw new ArgumentException("Wrong data type for this parameter");
            buffer[offset] = (Byte)(ParameterSetBase + item);
            Byte[] data = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian) Array.Reverse(data);
            Buffer.BlockCopy(data, 0, buffer, offset + 1, 2);
            return 3;
        }

        public static Int32 CreateParameterSetMessage(Byte[] buffer, Int32 offset, ParameterItem item, Int16 value) {
            if (GetOutgoingMessageDataType((Byte)item) != typeof(Int16))
                throw new ArgumentException("Wrong data type for this parameter");
            buffer[offset] = (Byte)(ParameterSetBase + item);
            Byte[] data = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian) Array.Reverse(data);
            Buffer.BlockCopy(data, 0, buffer, offset + 1, 2);
            return 3;
        }

        public static Int32 CreateParameterSetMessage(Byte[] buffer, Int32 offset, ParameterItem item, float value) {
            if (GetOutgoingMessageDataType((Byte)item) != typeof(float))
                throw new ArgumentException("Wrong data type for this parameter");
            buffer[offset] = (Byte)(ParameterSetBase + item);
            Byte[] data = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian) Array.Reverse(data);
            Buffer.BlockCopy(data, 0, buffer, offset + 1, 4);
            return 5;
        }

        public static Int32 CreateTelemetryReportRequest(Byte[] buffer, Int32 offset, TelemetryItem item) {
            buffer[offset] = (Byte)item;
            return 1;
        }

        /// <summary>Gets the function of a message sent by the Segway</summary>
        /// <param name="code">Segway-to-computer message type code</param>
        public static OutgoingMessageClass GetOutgoingMessageClass(Byte code) {
            if (code >= ParameterResponseBase && code <= ParameterResponseMax)
                return OutgoingMessageClass.ParameterResponse;
            if (code >= TelemetryReportBase && code <= TelemetryReportMax)
                return OutgoingMessageClass.TelemetryReport;
            return OutgoingMessageClass.Reserved;
        }

        /// <summary>Gets the data length associated with a message sent by the Segway</summary>
        /// <param name="code">Segway-to-computer message type code</param>
        /// <returns>Data length or -1 if reserved</returns>
        public static Int32 GetOutgoingMessageDataLength(Byte code) {
            switch (GetOutgoingMessageClass(code)) {
                case OutgoingMessageClass.ParameterResponse:
                    code -= ParameterResponseBase;
                    break;
                case OutgoingMessageClass.TelemetryReport:
                    code -= TelemetryReportBase;
                    break;
                default:
                    return -1;
            }
            if (code < FloatBlock) return 2;
            else return 4;
        }

        /// <summary>Gets the data type associated with a message sent by the Segway</summary>
        /// <param name="code">Segway-to-computer message type code</param>
        /// <returns>UInt16, Int16, float, or null</returns>
        public static Type GetOutgoingMessageDataType(Byte code) {
            switch (GetOutgoingMessageClass(code)) {
                case OutgoingMessageClass.ParameterResponse:
                    code -= ParameterResponseBase;
                    break;
                case OutgoingMessageClass.TelemetryReport:
                    code -= TelemetryReportBase;
                    break;
                default:
                    return null;
            }
            if (code < IntBlock) return typeof(UInt16);
            if (code < FloatBlock) return typeof(Int16);
            else return typeof(float);
        }

        public static Type GetParameterDataType(ParameterItem item) {
            return GetOutgoingMessageDataType((Byte)item);
        }
    }

    public class MessageDecoder
    {
        private LoggerDelegate logger;

        public MessageDecoder() { }

        public MessageDecoder(LoggerDelegate logger) {
            this.logger = logger;
        }

        public delegate void LoggerDelegate(String msg, Boolean isDetail);
        public EventHandler<MessageReceivedEventArgs> TelemetryReportReceived;
        public EventHandler<MessageReceivedEventArgs> ParameterReportReceived;

        /// <summary>Decode and dispatch messages</summary>
        /// <param name="messageData">Raw message data</param>
        /// <exception cref="InvalidDataException">messageData is not a valid
        /// series of messages</exception>
        public void ProcessMessages(Byte[] messageData) {
            Int32 offset = 0;
            while (offset < messageData.Length) {
                // Read the proper number of bytes associated with the message
                Byte type = messageData[offset++];
                Int32 dataLen = Message.GetOutgoingMessageDataLength(type);
                if (dataLen == -1) {
                    throw new InvalidDataException("Message received is reserved for future use");
                }
                if (offset + dataLen > messageData.Length) {
                    throw new InvalidDataException("Incomplete message received");
                }
                Byte[] data = new Byte[dataLen];
                Buffer.BlockCopy(messageData, offset, data, 0, dataLen);

                // Decode data and fire events
                DispatchMessage(type, data);

                offset += dataLen;
            }
        }

        private void DispatchMessage(Byte mc, Byte[] data) {
            EventHandler<MessageReceivedEventArgs> handler;
            switch (Message.GetOutgoingMessageClass(mc)) {
                case OutgoingMessageClass.ParameterResponse:
                    handler = ParameterReportReceived;
                    break;
                case OutgoingMessageClass.TelemetryReport:
                    handler = TelemetryReportReceived;
                    break;
                default:
                    Log("Message received is reserved for future use and not caught by first check", false);
                    return;
            }
            if (handler == null) return;

            if (BitConverter.IsLittleEndian) Array.Reverse(data);
            Type dataType = Message.GetOutgoingMessageDataType(mc);
            if (dataType == typeof(UInt16)) {
                if (BitConverter.IsLittleEndian) Array.Reverse(data);
                handler(this, new MessageReceivedEventArgs(mc, BitConverter.ToUInt16(data, 0)));
            } else if (dataType == typeof(Int16)) {
                handler(this, new MessageReceivedEventArgs(mc, BitConverter.ToInt16(data, 0)));
            } else if (dataType == typeof(float)) {
                handler(this, new MessageReceivedEventArgs(mc, BitConverter.ToSingle(data, 0)));
            } else {
                Log("Can't get type of message received", false);
            }
        }

        [System.Diagnostics.Conditional("DEBUG")]
        private void Log(String msg, Boolean debug) {
            if (logger != null) logger.Invoke(msg, debug);
        }
    }

    public class MessageReceivedEventArgs : EventArgs
    {
        private float floatVal;
        private Int16 int16Val;
        private UInt16 uint16Val;

        public MessageReceivedEventArgs(Byte mc) {
            MessageCode = mc;
            IsFloat = IsInt16 = IsUInt16 = false;
        }

        public MessageReceivedEventArgs(Byte mc, float v) {
            MessageCode = mc;
            floatVal = v;
            IsFloat = true;
            IsInt16 = IsUInt16 = false;
        }

        public MessageReceivedEventArgs(Byte mc, Int16 v) {
            MessageCode = mc;
            int16Val = v;
            IsInt16 = true;
            IsFloat = IsUInt16 = false;
        }

        public MessageReceivedEventArgs(Byte mc, UInt16 v) {
            MessageCode = mc;
            uint16Val = v;
            IsUInt16 = true;
            IsFloat = IsInt16 = false;
        }

        public float FloatValue {
            get {
                if (IsFloat) return floatVal;
                else throw new InvalidOperationException("Message data is not a float");
            }
        }

        public float Int16Value {
            get {
                if (IsInt16) return int16Val;
                else throw new InvalidOperationException("Message data is not an Int16");
            }
        }

        public float UInt16Value {
            get {
                if (IsUInt16) return uint16Val;
                else throw new InvalidOperationException("Message data is not a UInt16");
            }
        }

        public Byte MessageCode { get; private set; }
        public Boolean IsFloat { get; private set; }
        public Boolean IsInt16 { get; private set; }
        public Boolean IsUInt16 { get; private set; }
    }
}
