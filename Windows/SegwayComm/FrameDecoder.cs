﻿using System;
using System.IO;

namespace CapstoneSegway.Comm
{
    public class FrameDecoder
    {
        public const Int32 MaxRawFrameSize = 64;

        private Byte[] rxBuffer = new Byte[MaxRawFrameSize];
        private Int32 rxBufferLength = 0;
        private Boolean synced = false;
        private Byte[] lastPayload = new Byte[0];
        private LoggerDelegate logger;

        public FrameDecoder() { }

        public FrameDecoder(LoggerDelegate logger) {
            this.logger = logger;
        }

        public delegate void LoggerDelegate(String msg, Boolean isDetail);

        /// <summary>Occurs when a valid frame is received.</summary>
        public EventHandler FrameReceived;

        /// <summary>Gets the data bytes of the last valid payload received</summary>
        public Byte[] LastPayload {
            get {
                return lastPayload;
            }
        }

        /// <summary>Dallas (now Maxim) iButton 8-bit CRC calculation.</summary>
        /// <remarks>
        /// Polynomial: x^8 + x^5 + x^4 + 1 (0x8C)
        /// Initial value: 0x0
        /// 
        /// See http://www.maxim-ic.com/appnotes.cfm/appnote_number/27
        /// Test Vector {0x02, 0x1C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0xA2} -> 0x00
        /// </remarks>
        /// <param name="data">Data array</param>
        /// <param name="offset">The zero-based offset into data</param>
        /// <param name="length">The number of bytes to calculate the CRC on</param>
        /// <returns>CRC-8</returns>
        public static Byte CalculateCRC(Byte[] data, Int32 offset, Int32 length) {
            Byte crc = 0;
            for (Int32 i = offset; i < offset + length; i++) {
                crc = (Byte)(crc ^ data[i]);
                for (Int32 j = 0; j < 8; j++) {
                    if ((crc & 0x01) == 1) {
                        crc = (Byte)((crc >> 1) ^ 0x8C);
                    } else {
                        crc >>= 1;
                    }
                }
            }
            return crc;
        }

        /// <summary>Dallas (now Maxim) iButton 8-bit CRC calculation.</summary>
        /// <remarks>
        /// Polynomial: x^8 + x^5 + x^4 + 1 (0x8C)
        /// Initial value: 0x0
        /// 
        /// See http://www.maxim-ic.com/appnotes.cfm/appnote_number/27
        /// Test Vector {0x02, 0x1C, 0xB8, 0x01, 0x00, 0x00, 0x00, 0xA2} -> 0x00
        /// </remarks>
        /// <param name="data">Data array</param>
        /// <returns>CRC-8</returns>
        public static Byte CalculateCRC(Byte[] data) {
            return CalculateCRC(data, 0, data.Length);
        }

        /// <summary>Encode binary data into a frame.</summary>
        /// <param name="data">Source data array</param>
        /// <param name="dataLen">Length of source buffer</param>
        /// <param name="frameBuffer">Output array</param>
        /// <returns>Length of frame in bytes</returns>
        /// <exception cref="ArgumentOutOfRangeException">Data is too short or long</exception>
        public static Int32 CreateFrame(Byte[] data, Int32 dataLen, Byte[] frameBuffer) {
            if (dataLen < 1 || dataLen > MaxRawFrameSize - 2) throw new ArgumentOutOfRangeException("dataLen");

            Buffer.BlockCopy(data, 0, frameBuffer, 0, dataLen);
            Byte crc = FrameDecoder.CalculateCRC(data, 0, dataLen);
            frameBuffer[dataLen] = crc;

            Byte[] encoded = COBS.Encode(frameBuffer, 0, dataLen + 1);
            Buffer.BlockCopy(encoded, 0, frameBuffer, 0, dataLen + 2);

            return dataLen + 2;
        }

        /// <summary>Processes an incoming byte and dispatches FrameReceived event.</summary>
        /// <param name="b">Incoming data</param>
        /// <returns>Whether this byte completed a valid frame.</returns>
        public Boolean ConsumeByte(Byte b) {
            if (synced) {
                if (b == 0) { // Found end of frame
                    Boolean frameValid = DecodeAndValidateFrame();
                    rxBufferLength = 0;
                    if (frameValid) RaiseFrameReceived();
                    return frameValid;
                } else if (rxBufferLength == MaxRawFrameSize) { // Frame too long
                    Log("Frame too long; unsycned", false);
                    synced = false;
                } else { // Receive data
                    rxBuffer[rxBufferLength++] = b;
                }
            } else if (b == 0) { // Not synced; found start of frame
                synced = true;
                rxBufferLength = 0;
                Log("Synced", false);
            } else { // Not synced; not a frame delimiter
                // drop
                Log("Unsync drop: " + b.ToString("X2"), false);
            }

            return false;
        }

        /// <summary>Decodes the contents of rxBuffer and places the data, if valid, in LastPayload</summary>
        /// <returns>Whether frame was valid</returns>
        private Boolean DecodeAndValidateFrame() {
            // Check 3 byte min length: 1 byte cobs overhead + 1 data byte + 1 byte CRC
            if (rxBufferLength == 0) return false; // never log these because they are annoying when used as a keep-alive
            if (rxBufferLength < 3) {
                Log("Dropping short frame: " + BitConverter.ToString(rxBuffer, 0, rxBufferLength), false);
                return false;
            }

            Byte[] decodedFrame;
            try {
                decodedFrame = COBS.Decode(rxBuffer, 0, rxBufferLength);
            } catch (InvalidDataException) {
                Log("Dropping frame with encoding error: " + BitConverter.ToString(rxBuffer, 0, rxBufferLength), false);
                return false;
            }
            if (CalculateCRC(decodedFrame, 0, decodedFrame.Length - 1) != decodedFrame[decodedFrame.Length - 1]) {
                Log("Dropping frame with bad CRC: " + BitConverter.ToString(rxBuffer, 0, rxBufferLength), false);
                return false;
            }

            lastPayload = new Byte[decodedFrame.Length - 1];
            Buffer.BlockCopy(decodedFrame, 0, lastPayload, 0, decodedFrame.Length - 1);
            Log("Got valid frame: " + BitConverter.ToString(lastPayload), true);
            return true;
        }

        private void RaiseFrameReceived() {
            EventHandler handler = FrameReceived; // temp variable to avoid race with null check
            if (handler != null) FrameReceived(this, new EventArgs());
        }

        [System.Diagnostics.Conditional("DEBUG")]
        private void Log(String msg, Boolean debug) {
            if (logger != null) logger.Invoke(msg, debug);
        }
    }
}
