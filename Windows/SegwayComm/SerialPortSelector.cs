﻿using System;
using System.ComponentModel;
using System.IO.Ports;
using System.Windows.Forms;

namespace CapstoneSegway.Comm
{
    public partial class SerialPortSelector : UserControl
    {
        private Boolean _connected = false;

        public SerialPortSelector() {
            InitializeComponent();
        }

        public event EventHandler ConnectRequest;
        public event EventHandler DisconnectRequest;

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Boolean Connected {
            get { return _connected; }
            set {
                _connected = value;
                cbBaud.Enabled = cbPort.Enabled = !value;
                btnConnect.Text = value ? "Disconnect" : "Connect";
                btnConnect.Enabled = true;
            }
        }

        [DefaultValue(0)]
        public Int32 BaudRate {
            get { return Int32.Parse((cbBaud.SelectedItem ?? "0").ToString()); }
            set { cbBaud.SelectedItem = value.ToString(); }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public String PortName {
            get { return (cbPort.SelectedItem ?? String.Empty).ToString(); }
            set { cbPort.SelectedItem = value; }
        }

        private void btnConnect_Click(object sender, EventArgs e) {
            if (Connected) {
                btnConnect.Enabled = false;
                EventHandler handler = DisconnectRequest; // to avoid null check race
                if (handler != null) handler(this, new EventArgs());
            } else {
                if (BaudRate == 0 || PortName == "") return;
                btnConnect.Enabled = false;
                EventHandler handler = ConnectRequest; // to avoid null check race
                if (handler != null) handler(this, new EventArgs());
            }
        }

        private void cbPort_DropDown(object sender, EventArgs e) {
            updatePorts();
        }

        private void updatePorts() {
            object old = cbPort.SelectedItem;
            cbPort.Items.Clear();
            cbPort.Items.AddRange(SerialPort.GetPortNames());
            if (old != null && cbPort.Items.Contains(old)) {
                cbPort.SelectedItem = old;
            }
        }

        private void SerialPortSelector_Load(object sender, EventArgs e) {
            updatePorts();
            if (cbPort.Items.Count != 0) {
                cbPort.SelectedItem = cbPort.Items[0];
            }
        }
    }
}
