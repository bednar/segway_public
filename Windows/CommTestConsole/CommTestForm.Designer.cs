﻿namespace CapstoneSegway.CommTestConsole
{
    partial class CommTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.tbOut = new System.Windows.Forms.TextBox();
            this.serial = new System.IO.Ports.SerialPort(this.components);
            this.rbText = new System.Windows.Forms.RadioButton();
            this.rbFrames = new System.Windows.Forms.RadioButton();
            this.rbMessages = new System.Windows.Forms.RadioButton();
            this.btnClear = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnRawSend = new System.Windows.Forms.Button();
            this.tbRaw = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tbEncoded = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCheck = new System.Windows.Forms.TextBox();
            this.cbLeading = new System.Windows.Forms.CheckBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.tbData = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbParamData = new System.Windows.Forms.TextBox();
            this.btnSetParam = new System.Windows.Forms.Button();
            this.btnGetParam = new System.Windows.Forms.Button();
            this.cbParams = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnGetTelemetry = new System.Windows.Forms.Button();
            this.cbTelemetry = new System.Windows.Forms.ComboBox();
            this.cbDebug = new System.Windows.Forms.CheckBox();
            this.serialPortSelector = new CapstoneSegway.Comm.SerialPortSelector();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbOut
            // 
            this.tbOut.AcceptsReturn = true;
            this.tbOut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOut.BackColor = System.Drawing.SystemColors.Window;
            this.tbOut.Location = new System.Drawing.Point(0, 129);
            this.tbOut.Multiline = true;
            this.tbOut.Name = "tbOut";
            this.tbOut.ReadOnly = true;
            this.tbOut.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbOut.Size = new System.Drawing.Size(821, 378);
            this.tbOut.TabIndex = 2;
            // 
            // serial
            // 
            this.serial.ReadTimeout = 50;
            this.serial.WriteTimeout = 1000;
            this.serial.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serial_DataReceived);
            // 
            // rbText
            // 
            this.rbText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbText.AutoSize = true;
            this.rbText.Location = new System.Drawing.Point(12, 513);
            this.rbText.Name = "rbText";
            this.rbText.Size = new System.Drawing.Size(117, 17);
            this.rbText.TabIndex = 3;
            this.rbText.Text = "Escaped Plain Text";
            this.rbText.UseVisualStyleBackColor = true;
            // 
            // rbFrames
            // 
            this.rbFrames.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbFrames.AutoSize = true;
            this.rbFrames.Location = new System.Drawing.Point(136, 513);
            this.rbFrames.Name = "rbFrames";
            this.rbFrames.Size = new System.Drawing.Size(100, 17);
            this.rbFrames.TabIndex = 4;
            this.rbFrames.Text = "Decode Frames";
            this.rbFrames.UseVisualStyleBackColor = true;
            // 
            // rbMessages
            // 
            this.rbMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbMessages.AutoSize = true;
            this.rbMessages.Checked = true;
            this.rbMessages.Location = new System.Drawing.Point(242, 513);
            this.rbMessages.Name = "rbMessages";
            this.rbMessages.Size = new System.Drawing.Size(114, 17);
            this.rbMessages.TabIndex = 5;
            this.rbMessages.TabStop = true;
            this.rbMessages.Text = "Decode Messages";
            this.rbMessages.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(746, 510);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(11, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(640, 110);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnRawSend);
            this.tabPage1.Controls.Add(this.tbRaw);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(632, 84);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Raw";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnRawSend
            // 
            this.btnRawSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRawSend.Enabled = false;
            this.btnRawSend.Location = new System.Drawing.Point(551, 6);
            this.btnRawSend.Name = "btnRawSend";
            this.btnRawSend.Size = new System.Drawing.Size(75, 23);
            this.btnRawSend.TabIndex = 1;
            this.btnRawSend.Text = "Raw Send";
            this.btnRawSend.UseVisualStyleBackColor = true;
            this.btnRawSend.Click += new System.EventHandler(this.btnRawSend_Click);
            // 
            // tbRaw
            // 
            this.tbRaw.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRaw.Location = new System.Drawing.Point(6, 8);
            this.tbRaw.Name = "tbRaw";
            this.tbRaw.Size = new System.Drawing.Size(539, 20);
            this.tbRaw.TabIndex = 0;
            this.tbRaw.Leave += new System.EventHandler(this.tbRaw_Leave);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tbEncoded);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.tbCheck);
            this.tabPage2.Controls.Add(this.cbLeading);
            this.tabPage2.Controls.Add(this.btnSend);
            this.tabPage2.Controls.Add(this.tbData);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(632, 84);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Frames";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tbEncoded
            // 
            this.tbEncoded.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEncoded.Location = new System.Drawing.Point(90, 32);
            this.tbEncoded.Name = "tbEncoded";
            this.tbEncoded.ReadOnly = true;
            this.tbEncoded.Size = new System.Drawing.Size(427, 20);
            this.tbEncoded.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(523, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "\\0";
            // 
            // tbCheck
            // 
            this.tbCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCheck.Location = new System.Drawing.Point(475, 6);
            this.tbCheck.Name = "tbCheck";
            this.tbCheck.ReadOnly = true;
            this.tbCheck.Size = new System.Drawing.Size(42, 20);
            this.tbCheck.TabIndex = 2;
            // 
            // cbLeading
            // 
            this.cbLeading.AutoSize = true;
            this.cbLeading.Location = new System.Drawing.Point(6, 8);
            this.cbLeading.Name = "cbLeading";
            this.cbLeading.Size = new System.Drawing.Size(78, 17);
            this.cbLeading.TabIndex = 0;
            this.cbLeading.Text = "Leading \\0";
            this.cbLeading.UseVisualStyleBackColor = true;
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Enabled = false;
            this.btnSend.Location = new System.Drawing.Point(551, 6);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 4;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // tbData
            // 
            this.tbData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbData.Location = new System.Drawing.Point(90, 6);
            this.tbData.Name = "tbData";
            this.tbData.Size = new System.Drawing.Size(379, 20);
            this.tbData.TabIndex = 1;
            this.tbData.TextChanged += new System.EventHandler(this.tbData_TextChanged);
            this.tbData.Leave += new System.EventHandler(this.tbData_Leave);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(632, 84);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Messages";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1MinSize = 50;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2MinSize = 50;
            this.splitContainer1.Size = new System.Drawing.Size(626, 78);
            this.splitContainer1.SplitterDistance = 313;
            this.splitContainer1.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbParamData);
            this.groupBox1.Controls.Add(this.btnSetParam);
            this.groupBox1.Controls.Add(this.btnGetParam);
            this.groupBox1.Controls.Add(this.cbParams);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 78);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameter";
            // 
            // tbParamData
            // 
            this.tbParamData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbParamData.Location = new System.Drawing.Point(7, 47);
            this.tbParamData.Name = "tbParamData";
            this.tbParamData.Size = new System.Drawing.Size(219, 20);
            this.tbParamData.TabIndex = 2;
            // 
            // btnSetParam
            // 
            this.btnSetParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetParam.Enabled = false;
            this.btnSetParam.Location = new System.Drawing.Point(232, 47);
            this.btnSetParam.Name = "btnSetParam";
            this.btnSetParam.Size = new System.Drawing.Size(75, 24);
            this.btnSetParam.TabIndex = 3;
            this.btnSetParam.Text = "Set";
            this.btnSetParam.UseVisualStyleBackColor = true;
            this.btnSetParam.Click += new System.EventHandler(this.btnSetParam_Click);
            // 
            // btnGetParam
            // 
            this.btnGetParam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetParam.Enabled = false;
            this.btnGetParam.Location = new System.Drawing.Point(232, 18);
            this.btnGetParam.Name = "btnGetParam";
            this.btnGetParam.Size = new System.Drawing.Size(75, 24);
            this.btnGetParam.TabIndex = 1;
            this.btnGetParam.Text = "Get";
            this.btnGetParam.UseVisualStyleBackColor = true;
            this.btnGetParam.Click += new System.EventHandler(this.btnGetParam_Click);
            // 
            // cbParams
            // 
            this.cbParams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbParams.FormattingEnabled = true;
            this.cbParams.Location = new System.Drawing.Point(7, 20);
            this.cbParams.Name = "cbParams";
            this.cbParams.Size = new System.Drawing.Size(219, 21);
            this.cbParams.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnGetTelemetry);
            this.groupBox2.Controls.Add(this.cbTelemetry);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(309, 78);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Telemetry";
            // 
            // btnGetTelemetry
            // 
            this.btnGetTelemetry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetTelemetry.Enabled = false;
            this.btnGetTelemetry.Location = new System.Drawing.Point(231, 18);
            this.btnGetTelemetry.Name = "btnGetTelemetry";
            this.btnGetTelemetry.Size = new System.Drawing.Size(75, 24);
            this.btnGetTelemetry.TabIndex = 1;
            this.btnGetTelemetry.Text = "Get";
            this.btnGetTelemetry.UseVisualStyleBackColor = true;
            this.btnGetTelemetry.Click += new System.EventHandler(this.btnGetTelemetry_Click);
            // 
            // cbTelemetry
            // 
            this.cbTelemetry.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTelemetry.FormattingEnabled = true;
            this.cbTelemetry.Location = new System.Drawing.Point(6, 20);
            this.cbTelemetry.Name = "cbTelemetry";
            this.cbTelemetry.Size = new System.Drawing.Size(219, 21);
            this.cbTelemetry.TabIndex = 0;
            // 
            // cbDebug
            // 
            this.cbDebug.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbDebug.AutoSize = true;
            this.cbDebug.Location = new System.Drawing.Point(404, 513);
            this.cbDebug.Name = "cbDebug";
            this.cbDebug.Size = new System.Drawing.Size(126, 17);
            this.cbDebug.TabIndex = 6;
            this.cbDebug.Text = "Show detailed output";
            this.cbDebug.UseVisualStyleBackColor = true;
            // 
            // serialPortSelector
            // 
            this.serialPortSelector.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.serialPortSelector.BaudRate = 115200;
            this.serialPortSelector.Location = new System.Drawing.Point(657, 13);
            this.serialPortSelector.Name = "serialPortSelector";
            this.serialPortSelector.Size = new System.Drawing.Size(165, 110);
            this.serialPortSelector.TabIndex = 1;
            this.serialPortSelector.ConnectRequest += new System.EventHandler(this.serialPortSelector_ConnectRequest);
            this.serialPortSelector.DisconnectRequest += new System.EventHandler(this.serialPortSelector_DisconnectRequest);
            // 
            // CommTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 542);
            this.Controls.Add(this.cbDebug);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.rbMessages);
            this.Controls.Add(this.rbFrames);
            this.Controls.Add(this.rbText);
            this.Controls.Add(this.serialPortSelector);
            this.Controls.Add(this.tbOut);
            this.Controls.Add(this.tabControl1);
            this.Name = "CommTestForm";
            this.Text = "Comm Test";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbOut;
        private System.IO.Ports.SerialPort serial;
        private Comm.SerialPortSelector serialPortSelector;
        private System.Windows.Forms.RadioButton rbText;
        private System.Windows.Forms.RadioButton rbFrames;
        private System.Windows.Forms.RadioButton rbMessages;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnRawSend;
        private System.Windows.Forms.TextBox tbRaw;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tbEncoded;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCheck;
        private System.Windows.Forms.CheckBox cbLeading;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox tbData;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSetParam;
        private System.Windows.Forms.Button btnGetParam;
        private System.Windows.Forms.ComboBox cbParams;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnGetTelemetry;
        private System.Windows.Forms.ComboBox cbTelemetry;
        private System.Windows.Forms.TextBox tbParamData;
        private System.Windows.Forms.CheckBox cbDebug;
    }
}

