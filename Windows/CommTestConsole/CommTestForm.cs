﻿using System;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Windows.Forms;
using CapstoneSegway.Comm;

namespace CapstoneSegway.CommTestConsole
{
    public partial class CommTestForm : Form
    {
        FrameDecoder frameDecoder;
        MessageDecoder messageDecoder;

        public CommTestForm() {
            InitializeComponent();

#if DEBUG
            frameDecoder = new FrameDecoder(new FrameDecoder.LoggerDelegate(AppendTextLn));
            messageDecoder = new MessageDecoder(new MessageDecoder.LoggerDelegate(AppendTextLn));
#else
            frameDecoder = new FrameDecoder();
            messageDecoder = new MessageDecoder();
            AppendText("Internal details from the frame and message decoders are only available when compiled in debug mode.", false);
            cbDebug.Enabled = false;
            cbDebug.Checked = false;
#endif
            frameDecoder.FrameReceived += new EventHandler(FrameReceived);
            messageDecoder.ParameterReportReceived += new EventHandler<MessageReceivedEventArgs>(ParameterReportReceived);
            messageDecoder.TelemetryReportReceived += new EventHandler<MessageReceivedEventArgs>(TelemetryReportReceived);

            cbParams.DataSource = Enum.GetValues(typeof(ParameterItem));
            cbTelemetry.DataSource = Enum.GetValues(typeof(TelemetryItem));
        }

        #region Serial Connection Management

        private void serialPortSelector_ConnectRequest(object sender, EventArgs e) {
            using (new HourGlass()) {
                serial.PortName = serialPortSelector.PortName;
                serial.BaudRate = serialPortSelector.BaudRate;
                try {
                    serial.Open();
                } catch (Exception ex) {
                    MessageBox.Show(
                        ex.GetType().ToString() + Environment.NewLine + ex.Message,
                        "Connection Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                } finally {
                    UpdateConnectionState();
                }
            }
        }

        private void serialPortSelector_DisconnectRequest(object sender, EventArgs e) {
            using (new HourGlass()) {
                try {
                    serial.Close();
                } catch (Exception ex) {
                    MessageBox.Show(
                        ex.GetType().ToString() + Environment.NewLine + ex.Message,
                        "Disconnection Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                } finally {
                    UpdateConnectionState();
                }
            }
        }

        private void SafeDisconnect() {
            if (this.InvokeRequired) {
                this.BeginInvoke(new MethodInvoker(SafeDisconnect));
            } else {
                serialPortSelector_DisconnectRequest(null, EventArgs.Empty);
            }
        }

        private void UpdateConnectionState() {
            btnSend.Enabled = btnRawSend.Enabled = btnGetParam.Enabled = btnSetParam.Enabled = btnGetTelemetry.Enabled = serialPortSelector.Connected = serial.IsOpen;
        }

        #endregion

        #region TX

        private Boolean TryWriteFrame(byte[] buffer, int offset, int count, Boolean leadingNull, Boolean trailingNull) {
            try {
                if (leadingNull) serial.Write(new Byte[] { 0x00 }, 0, 1);
                serial.Write(buffer, offset, count);
                if (trailingNull) serial.Write(new Byte[] { 0x00 }, 0, 1);
                return true;
            } catch (TimeoutException) {
                AppendTextLn("Write operation timed out.", false);
            }
            // TODO: catch other exceptions?
            return false;
        }

        #region Frame TX

        private Byte[] HexStringToByteArray(String s) {
            String[] byteStrings = s.Split(new String[] { " ", "-" }, StringSplitOptions.RemoveEmptyEntries);
            Byte[] bytes = new Byte[byteStrings.Length];
            for (Int32 i = 0; i < byteStrings.Length; i++) {
                if (!Byte.TryParse(byteStrings[i], System.Globalization.NumberStyles.HexNumber, null, out bytes[i])) {
                    bytes[i] = 0;
                }
            }
            return bytes;
        }

        private void tbData_TextChanged(object sender, EventArgs e) {
            Byte[] data = HexStringToByteArray(tbData.Text);
            if (data.Length > 253) {
                tbEncoded.Text = "";
                tbCheck.Text = "";
                return;
            }

            Byte[] dataAndCRC = new Byte[data.Length + 1];
            Buffer.BlockCopy(data, 0, dataAndCRC, 0, data.Length);
            Byte crc = FrameDecoder.CalculateCRC(data);
            dataAndCRC[dataAndCRC.Length - 1] = crc;

            Byte[] encoded = COBS.Encode(dataAndCRC);
            tbEncoded.Text = BitConverter.ToString(encoded);
            tbCheck.Text = crc.ToString("X2");
        }

        private void tbData_Leave(object sender, EventArgs e) {
            tbData.Text = BitConverter.ToString(HexStringToByteArray(tbData.Text));
        }

        private void tbRaw_Leave(object sender, EventArgs e) {
            tbRaw.Text = BitConverter.ToString(HexStringToByteArray(tbRaw.Text));
        }

        private void btnRawSend_Click(object sender, EventArgs e) {
            Byte[] data = HexStringToByteArray(tbRaw.Text);
            TryWriteFrame(data, 0, data.Length, false, false);
        }

        private void btnSend_Click(object sender, EventArgs e) {
            Byte[] data = HexStringToByteArray(tbEncoded.Text);
            TryWriteFrame(data, 0, data.Length, cbLeading.Checked, true);
        }

        #endregion

        #region Message TX

        private void SendMessage(Byte[] message, Int32 length) {
            byte[] frameBuffer = new Byte[FrameDecoder.MaxRawFrameSize];
            length = FrameDecoder.CreateFrame(message, length, frameBuffer);
            TryWriteFrame(frameBuffer, 0, length, true, true);
        }

        private void btnGetParam_Click(object sender, EventArgs e) {
            ParameterItem item = (ParameterItem)Enum.Parse(typeof(ParameterItem), cbParams.SelectedItem.ToString());
            byte[] messageBuffer = new Byte[FrameDecoder.MaxRawFrameSize];
            Int32 len = Comm.Message.CreateParameterValueRequest(messageBuffer, 0, item);
            SendMessage(messageBuffer, len);
        }

        private void btnSetParam_Click(object sender, EventArgs e) {
            ParameterItem item = (ParameterItem)Enum.Parse(typeof(ParameterItem), cbParams.SelectedItem.ToString());
            Type type = Comm.Message.GetParameterDataType(item);
            byte[] messageBuffer = new Byte[FrameDecoder.MaxRawFrameSize];
            Int32 len;

            if (type == typeof(UInt16)) {
                UInt16 data;
                if (!UInt16.TryParse(tbParamData.Text, out data)) {
                    MessageBox.Show("Invalid UInt16");
                    return;
                }
                len = Comm.Message.CreateParameterSetMessage(messageBuffer, 0, item, data);
            } else if (type == typeof(Int16)) {
                Int16 data;
                if (!Int16.TryParse(tbParamData.Text, out data)) {
                    MessageBox.Show("Invalid Int16");
                    return;
                }
                len = Comm.Message.CreateParameterSetMessage(messageBuffer, 0, item, data);
            } else if (type == typeof(float)) {
                float data;
                if (!Single.TryParse(tbParamData.Text, out data)) {
                    MessageBox.Show("Invalid float");
                    return;
                }
                len = Comm.Message.CreateParameterSetMessage(messageBuffer, 0, item, data);
            } else {
                throw new NotImplementedException();
            }

            SendMessage(messageBuffer, len);
        }

        private void btnGetTelemetry_Click(object sender, EventArgs e) {
            TelemetryItem item = (TelemetryItem)Enum.Parse(typeof(TelemetryItem), cbTelemetry.SelectedItem.ToString());
            byte[] messageBuffer = new Byte[FrameDecoder.MaxRawFrameSize];
            Int32 len = Comm.Message.CreateTelemetryReportRequest(messageBuffer, 0, item);
            SendMessage(messageBuffer, len);
        }

        #endregion

        #endregion

        #region RX

        private void serial_DataReceived(object sender, SerialDataReceivedEventArgs e) {
            try {
                if (rbText.Checked) {
                    StringBuilder sb = new StringBuilder(serial.BytesToRead + 5);
                    while (serial.BytesToRead != 0) {
                        sb.Append(Escape((Byte)serial.ReadByte()));
                    }
                    AppendText(sb.ToString(), false);
                    return;
                }

                AppendTextLn("Start DataRecived handler", true);
                while (serial.BytesToRead != 0) {
                    frameDecoder.ConsumeByte((Byte)serial.ReadByte());
                }
                AppendTextLn("End DataRecived handler", true);

            } catch (InvalidOperationException ex) {
                MessageBox.Show(
                    "Error while reading data" + Environment.NewLine + ex.Message,
                    "Read Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                SafeDisconnect();
            }
        }

        private void FrameReceived(object sender, EventArgs e) {
            if (rbMessages.Checked) {
                AppendTextLn("Handle Frame", true);
                try {
                    messageDecoder.ProcessMessages(frameDecoder.LastPayload);
                } catch (InvalidDataException ex) {
                    AppendTextLn(ex.Message, false);
                }
                AppendTextLn("End frame handler", true);
                return;
            }

            if (rbFrames.Checked && !cbDebug.Checked) {
                // done within FrameDecoder when in detail is on
                AppendTextLn("Got valid frame: " + BitConverter.ToString(frameDecoder.LastPayload), false);
            }
        }

        private void ParameterReportReceived(object sender, MessageReceivedEventArgs e) {
            AppendText("Parameter report ", false);
            HandleReport(e, (ParameterItem)e.MessageCode);
        }

        private void TelemetryReportReceived(object sender, MessageReceivedEventArgs e) {
            AppendText("Telemetry report ", false);
            HandleReport(e, (TelemetryItem)e.MessageCode);
        }

        private void HandleReport(MessageReceivedEventArgs e, Object field) {
            if (e.IsFloat) AppendTextLn(String.Format("(float): {0}={1}", field, e.FloatValue), false);
            else if (e.IsInt16) AppendTextLn(String.Format("(Int16): {0}={1}", field, e.Int16Value), false);
            else if (e.IsUInt16) AppendTextLn(String.Format("(UInt16): {0}={1}", field, e.UInt16Value), false);
            else AppendTextLn(String.Format("(unknown data type): {0}", field), false);
        }

        #endregion

        #region Output box

        delegate void AppendTextCallback(String t, Boolean isDetail);

        private void AppendText(string t, Boolean isDetail) {
            if (tbOut.InvokeRequired) {
                this.BeginInvoke(new AppendTextCallback(AppendText), new Object[] { t, isDetail });
            } else {
                if (!isDetail || cbDebug.Checked) tbOut.AppendText(t);
            }
        }

        private void AppendTextLn(string t, Boolean isDetail) {
            AppendText(t + Environment.NewLine, isDetail);
        }

        private void btnClear_Click(object sender, EventArgs e) {
            tbOut.Text = "";
        }

        /// <summary>
        /// Does C style escaping but inserts a newline after \n
        /// </summary>
        private static string Escape(Byte b) {
            switch ((Char)b) {
                case '\'': return @"\'";
                case '\"': return "\\\"";
                case '\\': return @"\\";
                case '\0': return @"\0";
                case '\a': return @"\a";
                case '\b': return @"\b";
                case '\f': return @"\f";
                case '\n': return @"\n" + Environment.NewLine;
                case '\r': return @"\r";
                case '\t': return @"\t";
                case '\v': return @"\v";
                default:
                    if (b >= 0x20 && b <= 0x7e) {
                        return System.Text.ASCIIEncoding.ASCII.GetString(new Byte[] { b });
                    } else {
                        return @"\x" + ((int)b).ToString("X2");
                    }
            }
        }

        #endregion
    }
}
