﻿namespace CapstoneSegway.PID_Tuner
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnClear = new System.Windows.Forms.Button();
            this.cbPidArea = new System.Windows.Forms.CheckBox();
            this.cbPidLine = new System.Windows.Forms.CheckBox();
            this.cbAngle = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.Location = new System.Drawing.Point(12, 12);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(695, 451);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(631, 469);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // cbPidArea
            // 
            this.cbPidArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbPidArea.AutoSize = true;
            this.cbPidArea.Checked = true;
            this.cbPidArea.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPidArea.Location = new System.Drawing.Point(12, 473);
            this.cbPidArea.Name = "cbPidArea";
            this.cbPidArea.Size = new System.Drawing.Size(90, 17);
            this.cbPidArea.TabIndex = 1;
            this.cbPidArea.Text = "PID Area Plot";
            this.cbPidArea.UseVisualStyleBackColor = true;
            this.cbPidArea.CheckedChanged += new System.EventHandler(this.plotCheckboxes_CheckedChanged);
            // 
            // cbPidLine
            // 
            this.cbPidLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbPidLine.AutoSize = true;
            this.cbPidLine.Checked = true;
            this.cbPidLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPidLine.Location = new System.Drawing.Point(126, 473);
            this.cbPidLine.Name = "cbPidLine";
            this.cbPidLine.Size = new System.Drawing.Size(88, 17);
            this.cbPidLine.TabIndex = 2;
            this.cbPidLine.Text = "PID Line Plot";
            this.cbPidLine.UseVisualStyleBackColor = true;
            this.cbPidLine.CheckedChanged += new System.EventHandler(this.plotCheckboxes_CheckedChanged);
            // 
            // cbAngle
            // 
            this.cbAngle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbAngle.AutoSize = true;
            this.cbAngle.Checked = true;
            this.cbAngle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAngle.Location = new System.Drawing.Point(238, 473);
            this.cbAngle.Name = "cbAngle";
            this.cbAngle.Size = new System.Drawing.Size(74, 17);
            this.cbAngle.TabIndex = 3;
            this.cbAngle.Text = "Angle Plot";
            this.cbAngle.UseVisualStyleBackColor = true;
            this.cbAngle.CheckedChanged += new System.EventHandler(this.plotCheckboxes_CheckedChanged);
            // 
            // GraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 504);
            this.Controls.Add(this.cbAngle);
            this.Controls.Add(this.cbPidLine);
            this.Controls.Add(this.cbPidArea);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.chart1);
            this.Name = "GraphForm";
            this.Text = "SEGWAY Plot";
            this.Load += new System.EventHandler(this.GraphForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.CheckBox cbPidArea;
        private System.Windows.Forms.CheckBox cbPidLine;
        private System.Windows.Forms.CheckBox cbAngle;
    }
}