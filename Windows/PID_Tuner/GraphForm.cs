﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CapstoneSegway.PID_Tuner
{
    public partial class GraphForm : Form
    {
        private Series PComponentSeries, DComponentSeries, IComponentSeries;
        private Series PComponentLineSeries, DComponentLineSeries, IComponentLineSeries;
        private Series OutputSeries, OutputLineSeries;
        private Series EstAngleSeries, TargetAngleSeries;
        private Int32 frame = 0;

        public GraphForm() {
            InitializeComponent();
        }

        private void GraphForm_Load(object sender, EventArgs e) {
            // Component chart areas, legends, and titles
            ChartArea chartArea = new ChartArea("ComponentChartArea");
            chartArea.CursorX.IsUserSelectionEnabled = true;
            chartArea.CursorY.IsUserSelectionEnabled = true;
            chart1.ChartAreas.Add(chartArea);
            chartArea = new ChartArea("ComponentLineChartArea");
            chartArea.CursorX.IsUserSelectionEnabled = true;
            chartArea.CursorY.IsUserSelectionEnabled = true;
            chart1.ChartAreas.Add(chartArea);
            Legend legend = new Legend("ComponentLegend");
            legend.DockedToChartArea = "ComponentChartArea";
            legend.IsDockedInsideChartArea = false;
            legend.LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
            chart1.Legends.Add(legend);
            legend = new Legend("ComponentLineLegend");
            legend.DockedToChartArea = "ComponentLineChartArea";
            legend.IsDockedInsideChartArea = false;
            legend.LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
            chart1.Legends.Add(legend);
            Title title = new Title("PID Control (Area)");
            title.DockedToChartArea = "ComponentChartArea";
            chart1.Titles.Add(title);
            title = new Title("PID Control (Line)");
            title.DockedToChartArea = "ComponentLineChartArea";
            chart1.Titles.Add(title);

            // Component area and line series
            PComponentSeries = new Series("PComponentSeries");
            DComponentSeries = new Series("DComponentSeries");
            IComponentSeries = new Series("IComponentSeries");
            PComponentLineSeries = new Series("PComponentLineSeries");
            DComponentLineSeries = new Series("DComponentLineSeries");
            IComponentLineSeries = new Series("IComponentLineSeries");
            PComponentSeries.LegendText = PComponentLineSeries.LegendText = PComponentLineSeries.ToolTip = "Proportional component";
            DComponentSeries.LegendText = DComponentLineSeries.LegendText = DComponentLineSeries.ToolTip = "Derivative component";
            IComponentSeries.LegendText = IComponentLineSeries.LegendText = IComponentLineSeries.ToolTip = "Integral component";
            foreach (Series s in new Series[] { PComponentSeries, DComponentSeries, IComponentSeries }) {
                s.ChartType = SeriesChartType.StackedArea;
                s.ChartArea = "ComponentChartArea";
                s.Legend = "ComponentLegend";
                chart1.Series.Add(s);
            }
            foreach (Series s in new Series[] { PComponentLineSeries, DComponentLineSeries, IComponentLineSeries }) {
                s.ChartType = SeriesChartType.FastLine;
                s.ChartArea = "ComponentLineChartArea";
                s.Legend = "ComponentLineLegend";
                s.BorderWidth = 5;
                chart1.Series.Add(s);
            }

            // Total output series in both component chart areas
            OutputSeries = new Series("OutputSeries");
            OutputLineSeries = new Series("OutputLineSeries");
            OutputSeries.LegendText = OutputLineSeries.LegendText = "PID Output";
            OutputSeries.ChartType = OutputLineSeries.ChartType = SeriesChartType.FastLine;
            OutputSeries.ChartArea = "ComponentChartArea";
            OutputLineSeries.ChartArea = "ComponentLineChartArea";
            OutputSeries.Legend = "ComponentLegend";
            OutputLineSeries.Legend = "ComponentLineLegend";
            OutputSeries.Color = OutputLineSeries.Color = Color.DarkGreen;
            OutputSeries.BorderWidth = OutputLineSeries.BorderWidth = 5;
            chart1.Series.Add(OutputSeries);
            chart1.Series.Add(OutputLineSeries);

            // Angle chart area, legend, title
            chartArea = new ChartArea("AngleChartArea");
            chartArea.CursorX.IsUserSelectionEnabled = true;
            chartArea.CursorY.IsUserSelectionEnabled = true;
            chart1.ChartAreas.Add(chartArea);
            legend = new Legend("AngleLegend");
            legend.DockedToChartArea = "AngleChartArea";
            legend.IsDockedInsideChartArea = false;
            legend.LegendItemOrder = LegendItemOrder.ReversedSeriesOrder;
            chart1.Legends.Add(legend);
            title = new Title("Angle");
            title.DockedToChartArea = "AngleChartArea";
            chart1.Titles.Add(title);

            // Angle series
            EstAngleSeries = new Series("EstAngleSeries");
            TargetAngleSeries = new Series("TargetAngleSeries");
            EstAngleSeries.LegendText = EstAngleSeries.ToolTip = "Estimated Angle";
            TargetAngleSeries.LegendText = TargetAngleSeries.ToolTip = "Target Angle";
            foreach (Series s in new Series[] { EstAngleSeries, TargetAngleSeries }) {
                s.ChartType = SeriesChartType.FastLine;
                s.ChartArea = "AngleChartArea";
                s.Legend = "AngleLegend";
                s.BorderWidth = 5;
                chart1.Series.Add(s);
            }
        }

        public void AddComponentPoints(float p, float d, float i, float total) {
            PComponentSeries.Points.AddXY(frame, p);
            DComponentSeries.Points.AddXY(frame, d);
            IComponentSeries.Points.AddXY(frame, i);
            PComponentLineSeries.Points.AddXY(frame, p);
            DComponentLineSeries.Points.AddXY(frame, d);
            IComponentLineSeries.Points.AddXY(frame, i);
            OutputSeries.Points.AddXY(frame, total);
            OutputLineSeries.Points.AddXY(frame, total);
            frame++;
        }

        public void AddAnglePoints(float estAngle, float targetAngle) {
            EstAngleSeries.Points.AddXY(frame, estAngle);
            TargetAngleSeries.Points.AddXY(frame, targetAngle);
        }

        private void btnClear_Click(object sender, EventArgs e) {
            PComponentSeries.Points.Clear();
            DComponentSeries.Points.Clear();
            IComponentSeries.Points.Clear();
            OutputSeries.Points.Clear();
            PComponentLineSeries.Points.Clear();
            DComponentLineSeries.Points.Clear();
            IComponentLineSeries.Points.Clear();
            OutputLineSeries.Points.Clear();
            EstAngleSeries.Points.Clear();
            TargetAngleSeries.Points.Clear();
        }

        private void plotCheckboxes_CheckedChanged(object sender, EventArgs e) {
            chart1.ChartAreas["ComponentChartArea"].Visible = cbPidArea.Checked;
            chart1.ChartAreas["ComponentLineChartArea"].Visible = cbPidLine.Checked;
            chart1.ChartAreas["AngleChartArea"].Visible = cbAngle.Checked;
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams {
            get {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
    }
}
