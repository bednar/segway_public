﻿namespace CapstoneSegway.PID_Tuner
{
    partial class DriveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.nudMaxAngle = new System.Windows.Forms.NumericUpDown();
            this.nudMinAngle = new System.Windows.Forms.NumericUpDown();
            this.nudMinSteer = new System.Windows.Forms.NumericUpDown();
            this.nudMaxSteer = new System.Windows.Forms.NumericUpDown();
            this.lblAngle = new System.Windows.Forms.Label();
            this.lblSteer = new System.Windows.Forms.Label();
            this.cbBalance = new System.Windows.Forms.CheckBox();
            this.btnZero = new System.Windows.Forms.Button();
            this.nudYScale = new System.Windows.Forms.NumericUpDown();
            this.nudXScale = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinSteer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxSteer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXScale)).BeginInit();
            this.SuspendLayout();
            // 
            // nudMaxAngle
            // 
            this.nudMaxAngle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.nudMaxAngle.DecimalPlaces = 2;
            this.nudMaxAngle.Location = new System.Drawing.Point(213, 12);
            this.nudMaxAngle.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudMaxAngle.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.nudMaxAngle.Name = "nudMaxAngle";
            this.nudMaxAngle.Size = new System.Drawing.Size(51, 20);
            this.nudMaxAngle.TabIndex = 2;
            this.nudMaxAngle.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudMaxAngle.ValueChanged += new System.EventHandler(this.nudMinAngle_ValueChanged);
            // 
            // nudMinAngle
            // 
            this.nudMinAngle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.nudMinAngle.DecimalPlaces = 2;
            this.nudMinAngle.Enabled = false;
            this.nudMinAngle.Location = new System.Drawing.Point(221, 430);
            this.nudMinAngle.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudMinAngle.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.nudMinAngle.Name = "nudMinAngle";
            this.nudMinAngle.Size = new System.Drawing.Size(51, 20);
            this.nudMinAngle.TabIndex = 3;
            this.nudMinAngle.Value = new decimal(new int[] {
            3,
            0,
            0,
            -2147483648});
            this.nudMinAngle.ValueChanged += new System.EventHandler(this.nudMinAngle_ValueChanged);
            // 
            // nudMinSteer
            // 
            this.nudMinSteer.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nudMinSteer.Enabled = false;
            this.nudMinSteer.Location = new System.Drawing.Point(12, 221);
            this.nudMinSteer.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudMinSteer.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            -2147483648});
            this.nudMinSteer.Name = "nudMinSteer";
            this.nudMinSteer.Size = new System.Drawing.Size(51, 20);
            this.nudMinSteer.TabIndex = 4;
            this.nudMinSteer.Value = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.nudMinSteer.ValueChanged += new System.EventHandler(this.nudMinAngle_ValueChanged);
            // 
            // nudMaxSteer
            // 
            this.nudMaxSteer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.nudMaxSteer.Location = new System.Drawing.Point(421, 221);
            this.nudMaxSteer.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudMaxSteer.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            -2147483648});
            this.nudMaxSteer.Name = "nudMaxSteer";
            this.nudMaxSteer.Size = new System.Drawing.Size(51, 20);
            this.nudMaxSteer.TabIndex = 5;
            this.nudMaxSteer.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudMaxSteer.ValueChanged += new System.EventHandler(this.nudMinAngle_ValueChanged);
            // 
            // lblAngle
            // 
            this.lblAngle.AutoSize = true;
            this.lblAngle.Location = new System.Drawing.Point(9, 12);
            this.lblAngle.Name = "lblAngle";
            this.lblAngle.Size = new System.Drawing.Size(37, 13);
            this.lblAngle.TabIndex = 0;
            this.lblAngle.Tag = "Angle: {0}";
            this.lblAngle.Text = "Angle:";
            // 
            // lblSteer
            // 
            this.lblSteer.AutoSize = true;
            this.lblSteer.Location = new System.Drawing.Point(9, 34);
            this.lblSteer.Name = "lblSteer";
            this.lblSteer.Size = new System.Drawing.Size(35, 13);
            this.lblSteer.TabIndex = 1;
            this.lblSteer.Tag = "Steer: {0}";
            this.lblSteer.Text = "Steer:";
            // 
            // cbBalance
            // 
            this.cbBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbBalance.AutoSize = true;
            this.cbBalance.Location = new System.Drawing.Point(12, 433);
            this.cbBalance.Name = "cbBalance";
            this.cbBalance.Size = new System.Drawing.Size(118, 17);
            this.cbBalance.TabIndex = 6;
            this.cbBalance.Text = "Use &Balance Board";
            this.cbBalance.UseVisualStyleBackColor = true;
            this.cbBalance.CheckedChanged += new System.EventHandler(this.cbBalance_CheckedChanged);
            // 
            // btnZero
            // 
            this.btnZero.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZero.Enabled = false;
            this.btnZero.Location = new System.Drawing.Point(421, 427);
            this.btnZero.Name = "btnZero";
            this.btnZero.Size = new System.Drawing.Size(51, 23);
            this.btnZero.TabIndex = 9;
            this.btnZero.Text = "&Zero";
            this.btnZero.UseVisualStyleBackColor = true;
            this.btnZero.Click += new System.EventHandler(this.btnZero_Click);
            // 
            // nudYScale
            // 
            this.nudYScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nudYScale.DecimalPlaces = 1;
            this.nudYScale.Location = new System.Drawing.Point(421, 401);
            this.nudYScale.Name = "nudYScale";
            this.nudYScale.Size = new System.Drawing.Size(51, 20);
            this.nudYScale.TabIndex = 7;
            this.nudYScale.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // nudXScale
            // 
            this.nudXScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nudXScale.DecimalPlaces = 1;
            this.nudXScale.Location = new System.Drawing.Point(364, 430);
            this.nudXScale.Name = "nudXScale";
            this.nudXScale.Size = new System.Drawing.Size(51, 20);
            this.nudXScale.TabIndex = 8;
            this.nudXScale.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // DriveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 462);
            this.Controls.Add(this.nudXScale);
            this.Controls.Add(this.nudYScale);
            this.Controls.Add(this.btnZero);
            this.Controls.Add(this.cbBalance);
            this.Controls.Add(this.lblSteer);
            this.Controls.Add(this.lblAngle);
            this.Controls.Add(this.nudMaxSteer);
            this.Controls.Add(this.nudMinSteer);
            this.Controls.Add(this.nudMinAngle);
            this.Controls.Add(this.nudMaxAngle);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DriveForm";
            this.Text = "SEGWAY Controller";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DriveForm_FormClosing);
            this.Load += new System.EventHandler(this.DriveForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DriveForm_Paint);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DriveForm_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinSteer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxSteer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXScale)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nudMaxAngle;
        private System.Windows.Forms.NumericUpDown nudMinAngle;
        private System.Windows.Forms.NumericUpDown nudMinSteer;
        private System.Windows.Forms.NumericUpDown nudMaxSteer;
        private System.Windows.Forms.Label lblAngle;
        private System.Windows.Forms.Label lblSteer;
        private System.Windows.Forms.CheckBox cbBalance;
        private System.Windows.Forms.Button btnZero;
        private System.Windows.Forms.NumericUpDown nudYScale;
        private System.Windows.Forms.NumericUpDown nudXScale;
    }
}