﻿namespace CapstoneSegway.PID_Tuner
{
    partial class TunerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.GroupBox gbP;
            System.Windows.Forms.GroupBox gbD;
            System.Windows.Forms.GroupBox gbI;
            System.Windows.Forms.GroupBox gbAutoSend;
            System.Windows.Forms.GroupBox gbFilter;
            System.Windows.Forms.Label lblHigh;
            System.Windows.Forms.Label lblLow;
            System.Windows.Forms.GroupBox gbMotor;
            System.Windows.Forms.Label lblM2Boost;
            System.Windows.Forms.Label lblM1Boost;
            System.Windows.Forms.Label lblDeadZone;
            System.Windows.Forms.Label lblRoundUp;
            System.Windows.Forms.GroupBox gbOther;
            System.Windows.Forms.Label lblAccelOffset;
            System.Windows.Forms.Label lblIntLimit;
            System.Windows.Forms.Label lblOut1;
            System.Windows.Forms.Label lblOut2;
            System.Windows.Forms.Label lblOut5;
            System.Windows.Forms.Label lblOut4;
            System.Windows.Forms.Label lblOut6;
            System.Windows.Forms.Label lblOut9;
            System.Windows.Forms.Label lblOut8;
            System.Windows.Forms.Label lblOut7;
            System.Windows.Forms.Label lblOut3;
            this.cbMinP = new System.Windows.Forms.ComboBox();
            this.nudP = new System.Windows.Forms.NumericUpDown();
            this.cbMaxP = new System.Windows.Forms.ComboBox();
            this.tbP = new System.Windows.Forms.TrackBar();
            this.cbMinD = new System.Windows.Forms.ComboBox();
            this.nudD = new System.Windows.Forms.NumericUpDown();
            this.cbMaxD = new System.Windows.Forms.ComboBox();
            this.tbD = new System.Windows.Forms.TrackBar();
            this.cbMinI = new System.Windows.Forms.ComboBox();
            this.nudI = new System.Windows.Forms.NumericUpDown();
            this.cbMaxI = new System.Windows.Forms.ComboBox();
            this.tbI = new System.Windows.Forms.TrackBar();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.cbAutoSend = new System.Windows.Forms.CheckBox();
            this.cbFilterLinked = new System.Windows.Forms.CheckBox();
            this.nudLow = new System.Windows.Forms.NumericUpDown();
            this.nudHigh = new System.Windows.Forms.NumericUpDown();
            this.nudM2Boost = new System.Windows.Forms.NumericUpDown();
            this.nudM1Boost = new System.Windows.Forms.NumericUpDown();
            this.nudRound = new System.Windows.Forms.NumericUpDown();
            this.nudDead = new System.Windows.Forms.NumericUpDown();
            this.nudAccelOffset = new System.Windows.Forms.NumericUpDown();
            this.nudIntLimit = new System.Windows.Forms.NumericUpDown();
            this.serial = new System.IO.Ports.SerialPort(this.components);
            this.timoutTimer = new System.Windows.Forms.Timer(this.components);
            this.rerequestTimer = new System.Windows.Forms.Timer(this.components);
            this.gbOut = new System.Windows.Forms.GroupBox();
            this.outBattery = new System.Windows.Forms.TextBox();
            this.outM2 = new System.Windows.Forms.TextBox();
            this.outM1 = new System.Windows.Forms.TextBox();
            this.outOutput = new System.Windows.Forms.TextBox();
            this.outInt = new System.Windows.Forms.TextBox();
            this.outError = new System.Windows.Forms.TextBox();
            this.outAngle = new System.Windows.Forms.TextBox();
            this.outGyro = new System.Windows.Forms.TextBox();
            this.outAccelAngle = new System.Windows.Forms.TextBox();
            this.serialPortSelector = new CapstoneSegway.Comm.SerialPortSelector();
            this.batteryRequestTimer = new System.Windows.Forms.Timer(this.components);
            gbP = new System.Windows.Forms.GroupBox();
            gbD = new System.Windows.Forms.GroupBox();
            gbI = new System.Windows.Forms.GroupBox();
            gbAutoSend = new System.Windows.Forms.GroupBox();
            gbFilter = new System.Windows.Forms.GroupBox();
            lblHigh = new System.Windows.Forms.Label();
            lblLow = new System.Windows.Forms.Label();
            gbMotor = new System.Windows.Forms.GroupBox();
            lblM2Boost = new System.Windows.Forms.Label();
            lblM1Boost = new System.Windows.Forms.Label();
            lblDeadZone = new System.Windows.Forms.Label();
            lblRoundUp = new System.Windows.Forms.Label();
            gbOther = new System.Windows.Forms.GroupBox();
            lblAccelOffset = new System.Windows.Forms.Label();
            lblIntLimit = new System.Windows.Forms.Label();
            lblOut1 = new System.Windows.Forms.Label();
            lblOut2 = new System.Windows.Forms.Label();
            lblOut5 = new System.Windows.Forms.Label();
            lblOut4 = new System.Windows.Forms.Label();
            lblOut6 = new System.Windows.Forms.Label();
            lblOut9 = new System.Windows.Forms.Label();
            lblOut8 = new System.Windows.Forms.Label();
            lblOut7 = new System.Windows.Forms.Label();
            lblOut3 = new System.Windows.Forms.Label();
            gbP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbP)).BeginInit();
            gbD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbD)).BeginInit();
            gbI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbI)).BeginInit();
            gbAutoSend.SuspendLayout();
            gbFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHigh)).BeginInit();
            gbMotor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudM2Boost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudM1Boost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDead)).BeginInit();
            gbOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAccelOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIntLimit)).BeginInit();
            this.gbOut.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbP
            // 
            gbP.Controls.Add(this.cbMinP);
            gbP.Controls.Add(this.nudP);
            gbP.Controls.Add(this.cbMaxP);
            gbP.Controls.Add(this.tbP);
            gbP.Location = new System.Drawing.Point(13, 12);
            gbP.Name = "gbP";
            gbP.Size = new System.Drawing.Size(106, 546);
            gbP.TabIndex = 0;
            gbP.TabStop = false;
            gbP.Text = "&Proportional";
            // 
            // cbMinP
            // 
            this.cbMinP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMinP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMinP.FormattingEnabled = true;
            this.cbMinP.Items.AddRange(new object[] {
            "500",
            "200",
            "100",
            "50",
            "20",
            "10",
            "0"});
            this.cbMinP.Location = new System.Drawing.Point(22, 518);
            this.cbMinP.Name = "cbMinP";
            this.cbMinP.Size = new System.Drawing.Size(63, 21);
            this.cbMinP.TabIndex = 3;
            this.cbMinP.Tag = 0;
            this.cbMinP.SelectionChangeCommitted += new System.EventHandler(this.cbMin_SelectionChangeCommitted);
            // 
            // nudP
            // 
            this.nudP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudP.DecimalPlaces = 3;
            this.nudP.Location = new System.Drawing.Point(7, 15);
            this.nudP.Name = "nudP";
            this.nudP.Size = new System.Drawing.Size(94, 20);
            this.nudP.TabIndex = 0;
            this.nudP.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.nudP.ValueChanged += new System.EventHandler(this.nudK_ValueChanged);
            // 
            // cbMaxP
            // 
            this.cbMaxP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMaxP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaxP.Items.AddRange(new object[] {
            "1000",
            "500",
            "200",
            "100",
            "50",
            "20",
            "10"});
            this.cbMaxP.Location = new System.Drawing.Point(22, 41);
            this.cbMaxP.Name = "cbMaxP";
            this.cbMaxP.Size = new System.Drawing.Size(63, 21);
            this.cbMaxP.TabIndex = 1;
            this.cbMaxP.Tag = 0;
            this.cbMaxP.SelectionChangeCommitted += new System.EventHandler(this.cbMax_SelectionChangeCommitted);
            // 
            // tbP
            // 
            this.tbP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbP.LargeChange = 10;
            this.tbP.Location = new System.Drawing.Point(32, 68);
            this.tbP.Maximum = 100;
            this.tbP.Name = "tbP";
            this.tbP.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbP.Size = new System.Drawing.Size(45, 444);
            this.tbP.SmallChange = 2;
            this.tbP.TabIndex = 2;
            this.tbP.Tag = 0;
            this.tbP.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbP.ValueChanged += new System.EventHandler(this.tb_ValueChanged);
            // 
            // gbD
            // 
            gbD.Controls.Add(this.cbMinD);
            gbD.Controls.Add(this.nudD);
            gbD.Controls.Add(this.cbMaxD);
            gbD.Controls.Add(this.tbD);
            gbD.Location = new System.Drawing.Point(125, 12);
            gbD.Name = "gbD";
            gbD.Size = new System.Drawing.Size(106, 546);
            gbD.TabIndex = 1;
            gbD.TabStop = false;
            gbD.Text = "&Derivative";
            // 
            // cbMinD
            // 
            this.cbMinD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMinD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMinD.FormattingEnabled = true;
            this.cbMinD.Items.AddRange(new object[] {
            "100",
            "50",
            "20",
            "10",
            "5",
            "1",
            "0.1",
            "0"});
            this.cbMinD.Location = new System.Drawing.Point(22, 518);
            this.cbMinD.Name = "cbMinD";
            this.cbMinD.Size = new System.Drawing.Size(63, 21);
            this.cbMinD.TabIndex = 3;
            this.cbMinD.Tag = 1;
            this.cbMinD.SelectionChangeCommitted += new System.EventHandler(this.cbMin_SelectionChangeCommitted);
            // 
            // nudD
            // 
            this.nudD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudD.DecimalPlaces = 3;
            this.nudD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudD.Location = new System.Drawing.Point(7, 15);
            this.nudD.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudD.Name = "nudD";
            this.nudD.Size = new System.Drawing.Size(94, 20);
            this.nudD.TabIndex = 0;
            this.nudD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudD.ValueChanged += new System.EventHandler(this.nudK_ValueChanged);
            // 
            // cbMaxD
            // 
            this.cbMaxD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMaxD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaxD.Items.AddRange(new object[] {
            "500",
            "100",
            "50",
            "20",
            "10",
            "5",
            "2",
            "1",
            "0.5"});
            this.cbMaxD.Location = new System.Drawing.Point(22, 41);
            this.cbMaxD.Name = "cbMaxD";
            this.cbMaxD.Size = new System.Drawing.Size(63, 21);
            this.cbMaxD.TabIndex = 1;
            this.cbMaxD.Tag = 1;
            this.cbMaxD.SelectionChangeCommitted += new System.EventHandler(this.cbMax_SelectionChangeCommitted);
            // 
            // tbD
            // 
            this.tbD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbD.LargeChange = 10;
            this.tbD.Location = new System.Drawing.Point(32, 68);
            this.tbD.Maximum = 100;
            this.tbD.Name = "tbD";
            this.tbD.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbD.Size = new System.Drawing.Size(45, 444);
            this.tbD.SmallChange = 2;
            this.tbD.TabIndex = 2;
            this.tbD.Tag = 1;
            this.tbD.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbD.ValueChanged += new System.EventHandler(this.tb_ValueChanged);
            // 
            // gbI
            // 
            gbI.Controls.Add(this.cbMinI);
            gbI.Controls.Add(this.nudI);
            gbI.Controls.Add(this.cbMaxI);
            gbI.Controls.Add(this.tbI);
            gbI.Location = new System.Drawing.Point(237, 12);
            gbI.Name = "gbI";
            gbI.Size = new System.Drawing.Size(106, 546);
            gbI.TabIndex = 2;
            gbI.TabStop = false;
            gbI.Text = "&Integral";
            // 
            // cbMinI
            // 
            this.cbMinI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMinI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMinI.FormattingEnabled = true;
            this.cbMinI.Items.AddRange(new object[] {
            "500",
            "200",
            "100",
            "50",
            "20",
            "10",
            "0"});
            this.cbMinI.Location = new System.Drawing.Point(22, 518);
            this.cbMinI.Name = "cbMinI";
            this.cbMinI.Size = new System.Drawing.Size(63, 21);
            this.cbMinI.TabIndex = 3;
            this.cbMinI.Tag = 2;
            this.cbMinI.SelectionChangeCommitted += new System.EventHandler(this.cbMin_SelectionChangeCommitted);
            // 
            // nudI
            // 
            this.nudI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudI.DecimalPlaces = 3;
            this.nudI.Location = new System.Drawing.Point(7, 15);
            this.nudI.Name = "nudI";
            this.nudI.Size = new System.Drawing.Size(94, 20);
            this.nudI.TabIndex = 0;
            this.nudI.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudI.ValueChanged += new System.EventHandler(this.nudK_ValueChanged);
            // 
            // cbMaxI
            // 
            this.cbMaxI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbMaxI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaxI.Items.AddRange(new object[] {
            "1500",
            "1000",
            "500",
            "200",
            "100",
            "50",
            "20",
            "10"});
            this.cbMaxI.Location = new System.Drawing.Point(22, 41);
            this.cbMaxI.Name = "cbMaxI";
            this.cbMaxI.Size = new System.Drawing.Size(63, 21);
            this.cbMaxI.TabIndex = 1;
            this.cbMaxI.Tag = 2;
            this.cbMaxI.SelectionChangeCommitted += new System.EventHandler(this.cbMax_SelectionChangeCommitted);
            // 
            // tbI
            // 
            this.tbI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbI.LargeChange = 10;
            this.tbI.Location = new System.Drawing.Point(32, 68);
            this.tbI.Maximum = 100;
            this.tbI.Name = "tbI";
            this.tbI.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbI.Size = new System.Drawing.Size(45, 444);
            this.tbI.SmallChange = 2;
            this.tbI.TabIndex = 2;
            this.tbI.Tag = 2;
            this.tbI.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbI.ValueChanged += new System.EventHandler(this.tb_ValueChanged);
            // 
            // gbAutoSend
            // 
            gbAutoSend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gbAutoSend.Controls.Add(this.btnLoad);
            gbAutoSend.Controls.Add(this.btnSend);
            gbAutoSend.Controls.Add(this.cbAutoSend);
            gbAutoSend.Location = new System.Drawing.Point(350, 129);
            gbAutoSend.Name = "gbAutoSend";
            gbAutoSend.Size = new System.Drawing.Size(192, 108);
            gbAutoSend.TabIndex = 4;
            gbAutoSend.TabStop = false;
            gbAutoSend.Text = "Update Control";
            // 
            // btnLoad
            // 
            this.btnLoad.Enabled = false;
            this.btnLoad.Location = new System.Drawing.Point(9, 50);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(177, 25);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.Text = "Load from Segway";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSend
            // 
            this.btnSend.Enabled = false;
            this.btnSend.Location = new System.Drawing.Point(9, 19);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(177, 25);
            this.btnSend.TabIndex = 0;
            this.btnSend.Text = "&Send all parameters";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // cbAutoSend
            // 
            this.cbAutoSend.AutoSize = true;
            this.cbAutoSend.Checked = true;
            this.cbAutoSend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoSend.Location = new System.Drawing.Point(9, 81);
            this.cbAutoSend.Name = "cbAutoSend";
            this.cbAutoSend.Size = new System.Drawing.Size(125, 17);
            this.cbAutoSend.TabIndex = 2;
            this.cbAutoSend.Text = "&Auto-send on update";
            this.cbAutoSend.UseVisualStyleBackColor = true;
            // 
            // gbFilter
            // 
            gbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gbFilter.Controls.Add(this.cbFilterLinked);
            gbFilter.Controls.Add(this.nudLow);
            gbFilter.Controls.Add(lblHigh);
            gbFilter.Controls.Add(lblLow);
            gbFilter.Controls.Add(this.nudHigh);
            gbFilter.Location = new System.Drawing.Point(350, 243);
            gbFilter.Name = "gbFilter";
            gbFilter.Size = new System.Drawing.Size(193, 98);
            gbFilter.TabIndex = 5;
            gbFilter.TabStop = false;
            gbFilter.Text = "&Complementary Filter Parameters";
            // 
            // cbFilterLinked
            // 
            this.cbFilterLinked.AutoSize = true;
            this.cbFilterLinked.Checked = true;
            this.cbFilterLinked.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFilterLinked.Location = new System.Drawing.Point(9, 71);
            this.cbFilterLinked.Name = "cbFilterLinked";
            this.cbFilterLinked.Size = new System.Drawing.Size(58, 17);
            this.cbFilterLinked.TabIndex = 4;
            this.cbFilterLinked.Text = "Linked";
            this.cbFilterLinked.UseVisualStyleBackColor = true;
            this.cbFilterLinked.CheckedChanged += new System.EventHandler(this.cbFilterLinked_CheckedChanged);
            // 
            // nudLow
            // 
            this.nudLow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudLow.DecimalPlaces = 5;
            this.nudLow.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.nudLow.Location = new System.Drawing.Point(78, 45);
            this.nudLow.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            65536});
            this.nudLow.Name = "nudLow";
            this.nudLow.Size = new System.Drawing.Size(109, 20);
            this.nudLow.TabIndex = 3;
            this.nudLow.Value = new decimal(new int[] {
            5,
            0,
            0,
            196608});
            this.nudLow.ValueChanged += new System.EventHandler(this.nudFilters_ValueChanged);
            // 
            // lblHigh
            // 
            lblHigh.AutoSize = true;
            lblHigh.Location = new System.Drawing.Point(6, 21);
            lblHigh.Name = "lblHigh";
            lblHigh.Size = new System.Drawing.Size(57, 13);
            lblHigh.TabIndex = 0;
            lblHigh.Text = "&High pass:";
            // 
            // lblLow
            // 
            lblLow.AutoSize = true;
            lblLow.Location = new System.Drawing.Point(6, 47);
            lblLow.Name = "lblLow";
            lblLow.Size = new System.Drawing.Size(55, 13);
            lblLow.TabIndex = 2;
            lblLow.Text = "&Low pass:";
            // 
            // nudHigh
            // 
            this.nudHigh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudHigh.DecimalPlaces = 5;
            this.nudHigh.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.nudHigh.Location = new System.Drawing.Point(78, 19);
            this.nudHigh.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            65536});
            this.nudHigh.Name = "nudHigh";
            this.nudHigh.Size = new System.Drawing.Size(109, 20);
            this.nudHigh.TabIndex = 1;
            this.nudHigh.Value = new decimal(new int[] {
            995,
            0,
            0,
            196608});
            this.nudHigh.ValueChanged += new System.EventHandler(this.nudFilters_ValueChanged);
            // 
            // gbMotor
            // 
            gbMotor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gbMotor.Controls.Add(this.nudM2Boost);
            gbMotor.Controls.Add(lblM2Boost);
            gbMotor.Controls.Add(this.nudM1Boost);
            gbMotor.Controls.Add(lblM1Boost);
            gbMotor.Controls.Add(this.nudRound);
            gbMotor.Controls.Add(lblDeadZone);
            gbMotor.Controls.Add(lblRoundUp);
            gbMotor.Controls.Add(this.nudDead);
            gbMotor.Location = new System.Drawing.Point(350, 347);
            gbMotor.Name = "gbMotor";
            gbMotor.Size = new System.Drawing.Size(193, 129);
            gbMotor.TabIndex = 6;
            gbMotor.TabStop = false;
            gbMotor.Text = "&Motor Parameters";
            // 
            // nudM2Boost
            // 
            this.nudM2Boost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudM2Boost.Location = new System.Drawing.Point(78, 97);
            this.nudM2Boost.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nudM2Boost.Minimum = new decimal(new int[] {
            400,
            0,
            0,
            -2147483648});
            this.nudM2Boost.Name = "nudM2Boost";
            this.nudM2Boost.Size = new System.Drawing.Size(109, 20);
            this.nudM2Boost.TabIndex = 7;
            this.nudM2Boost.ValueChanged += new System.EventHandler(this.nud_ValueChanged);
            // 
            // lblM2Boost
            // 
            lblM2Boost.AutoSize = true;
            lblM2Boost.Location = new System.Drawing.Point(6, 99);
            lblM2Boost.Name = "lblM2Boost";
            lblM2Boost.Size = new System.Drawing.Size(55, 13);
            lblM2Boost.TabIndex = 6;
            lblM2Boost.Text = "M2 Boost:";
            // 
            // nudM1Boost
            // 
            this.nudM1Boost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudM1Boost.Location = new System.Drawing.Point(78, 71);
            this.nudM1Boost.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nudM1Boost.Minimum = new decimal(new int[] {
            400,
            0,
            0,
            -2147483648});
            this.nudM1Boost.Name = "nudM1Boost";
            this.nudM1Boost.Size = new System.Drawing.Size(109, 20);
            this.nudM1Boost.TabIndex = 5;
            this.nudM1Boost.ValueChanged += new System.EventHandler(this.nud_ValueChanged);
            // 
            // lblM1Boost
            // 
            lblM1Boost.AutoSize = true;
            lblM1Boost.Location = new System.Drawing.Point(6, 73);
            lblM1Boost.Name = "lblM1Boost";
            lblM1Boost.Size = new System.Drawing.Size(55, 13);
            lblM1Boost.TabIndex = 4;
            lblM1Boost.Text = "M1 Boost:";
            // 
            // nudRound
            // 
            this.nudRound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudRound.Location = new System.Drawing.Point(78, 45);
            this.nudRound.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nudRound.Name = "nudRound";
            this.nudRound.Size = new System.Drawing.Size(109, 20);
            this.nudRound.TabIndex = 3;
            this.nudRound.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudRound.ValueChanged += new System.EventHandler(this.nud_ValueChanged);
            // 
            // lblDeadZone
            // 
            lblDeadZone.AutoSize = true;
            lblDeadZone.Location = new System.Drawing.Point(6, 21);
            lblDeadZone.Name = "lblDeadZone";
            lblDeadZone.Size = new System.Drawing.Size(62, 13);
            lblDeadZone.TabIndex = 0;
            lblDeadZone.Text = "Dead &zone:";
            // 
            // lblRoundUp
            // 
            lblRoundUp.AutoSize = true;
            lblRoundUp.Location = new System.Drawing.Point(6, 47);
            lblRoundUp.Name = "lblRoundUp";
            lblRoundUp.Size = new System.Drawing.Size(57, 13);
            lblRoundUp.TabIndex = 2;
            lblRoundUp.Text = "&Round up:";
            // 
            // nudDead
            // 
            this.nudDead.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudDead.Location = new System.Drawing.Point(78, 19);
            this.nudDead.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.nudDead.Name = "nudDead";
            this.nudDead.Size = new System.Drawing.Size(109, 20);
            this.nudDead.TabIndex = 1;
            this.nudDead.ValueChanged += new System.EventHandler(this.nud_ValueChanged);
            // 
            // gbOther
            // 
            gbOther.Controls.Add(lblAccelOffset);
            gbOther.Controls.Add(this.nudAccelOffset);
            gbOther.Controls.Add(lblIntLimit);
            gbOther.Controls.Add(this.nudIntLimit);
            gbOther.Location = new System.Drawing.Point(350, 482);
            gbOther.Name = "gbOther";
            gbOther.Size = new System.Drawing.Size(193, 76);
            gbOther.TabIndex = 7;
            gbOther.TabStop = false;
            gbOther.Text = "Other Parameters";
            // 
            // lblAccelOffset
            // 
            lblAccelOffset.AutoSize = true;
            lblAccelOffset.Location = new System.Drawing.Point(6, 47);
            lblAccelOffset.Name = "lblAccelOffset";
            lblAccelOffset.Size = new System.Drawing.Size(66, 13);
            lblAccelOffset.TabIndex = 2;
            lblAccelOffset.Text = "Accel offset:";
            // 
            // nudAccelOffset
            // 
            this.nudAccelOffset.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudAccelOffset.DecimalPlaces = 3;
            this.nudAccelOffset.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudAccelOffset.Location = new System.Drawing.Point(78, 45);
            this.nudAccelOffset.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudAccelOffset.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            -2147483648});
            this.nudAccelOffset.Name = "nudAccelOffset";
            this.nudAccelOffset.Size = new System.Drawing.Size(109, 20);
            this.nudAccelOffset.TabIndex = 3;
            this.nudAccelOffset.ValueChanged += new System.EventHandler(this.nud_ValueChanged);
            // 
            // lblIntLimit
            // 
            lblIntLimit.AutoSize = true;
            lblIntLimit.Location = new System.Drawing.Point(6, 21);
            lblIntLimit.Name = "lblIntLimit";
            lblIntLimit.Size = new System.Drawing.Size(65, 13);
            lblIntLimit.TabIndex = 0;
            lblIntLimit.Text = "Integral limit:";
            // 
            // nudIntLimit
            // 
            this.nudIntLimit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nudIntLimit.DecimalPlaces = 3;
            this.nudIntLimit.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nudIntLimit.Location = new System.Drawing.Point(78, 19);
            this.nudIntLimit.Name = "nudIntLimit";
            this.nudIntLimit.Size = new System.Drawing.Size(109, 20);
            this.nudIntLimit.TabIndex = 1;
            this.nudIntLimit.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nudIntLimit.ValueChanged += new System.EventHandler(this.nud_ValueChanged);
            // 
            // lblOut1
            // 
            lblOut1.AutoSize = true;
            lblOut1.Location = new System.Drawing.Point(7, 20);
            lblOut1.Name = "lblOut1";
            lblOut1.Size = new System.Drawing.Size(64, 13);
            lblOut1.TabIndex = 0;
            lblOut1.Text = "Accel Angle";
            lblOut1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOut2
            // 
            lblOut2.AutoSize = true;
            lblOut2.Location = new System.Drawing.Point(42, 46);
            lblOut2.Name = "lblOut2";
            lblOut2.Size = new System.Drawing.Size(29, 13);
            lblOut2.TabIndex = 2;
            lblOut2.Text = "Gyro";
            lblOut2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOut5
            // 
            lblOut5.AutoSize = true;
            lblOut5.Location = new System.Drawing.Point(191, 46);
            lblOut5.Name = "lblOut5";
            lblOut5.Size = new System.Drawing.Size(47, 13);
            lblOut5.TabIndex = 8;
            lblOut5.Text = "Est Error";
            lblOut5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOut4
            // 
            lblOut4.AutoSize = true;
            lblOut4.Location = new System.Drawing.Point(186, 20);
            lblOut4.Name = "lblOut4";
            lblOut4.Size = new System.Drawing.Size(52, 13);
            lblOut4.TabIndex = 6;
            lblOut4.Text = "Est Angle";
            lblOut4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOut6
            // 
            lblOut6.AutoSize = true;
            lblOut6.Location = new System.Drawing.Point(194, 72);
            lblOut6.Name = "lblOut6";
            lblOut6.Size = new System.Drawing.Size(44, 13);
            lblOut6.TabIndex = 10;
            lblOut6.Text = "Int Error";
            lblOut6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOut9
            // 
            lblOut9.AutoSize = true;
            lblOut9.Location = new System.Drawing.Point(387, 72);
            lblOut9.Name = "lblOut9";
            lblOut9.Size = new System.Drawing.Size(22, 13);
            lblOut9.TabIndex = 16;
            lblOut9.Text = "M2";
            lblOut9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOut8
            // 
            lblOut8.AutoSize = true;
            lblOut8.Location = new System.Drawing.Point(387, 46);
            lblOut8.Name = "lblOut8";
            lblOut8.Size = new System.Drawing.Size(22, 13);
            lblOut8.TabIndex = 14;
            lblOut8.Text = "M1";
            lblOut8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOut7
            // 
            lblOut7.AutoSize = true;
            lblOut7.Location = new System.Drawing.Point(370, 20);
            lblOut7.Name = "lblOut7";
            lblOut7.Size = new System.Drawing.Size(39, 13);
            lblOut7.TabIndex = 12;
            lblOut7.Text = "Output";
            lblOut7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOut3
            // 
            lblOut3.AutoSize = true;
            lblOut3.Location = new System.Drawing.Point(31, 72);
            lblOut3.Name = "lblOut3";
            lblOut3.Size = new System.Drawing.Size(40, 13);
            lblOut3.TabIndex = 4;
            lblOut3.Text = "Battery";
            lblOut3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // serial
            // 
            this.serial.ReadTimeout = 50;
            this.serial.WriteTimeout = 1000;
            this.serial.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serial_DataReceived);
            // 
            // timoutTimer
            // 
            this.timoutTimer.Interval = 1000;
            this.timoutTimer.Tick += new System.EventHandler(this.timoutTimer_Tick);
            // 
            // rerequestTimer
            // 
            this.rerequestTimer.Interval = 200;
            this.rerequestTimer.Tick += new System.EventHandler(this.rerequestTimer_Tick);
            // 
            // gbOut
            // 
            this.gbOut.Controls.Add(this.outBattery);
            this.gbOut.Controls.Add(lblOut3);
            this.gbOut.Controls.Add(this.outM2);
            this.gbOut.Controls.Add(lblOut9);
            this.gbOut.Controls.Add(this.outM1);
            this.gbOut.Controls.Add(lblOut8);
            this.gbOut.Controls.Add(this.outOutput);
            this.gbOut.Controls.Add(lblOut7);
            this.gbOut.Controls.Add(this.outInt);
            this.gbOut.Controls.Add(lblOut6);
            this.gbOut.Controls.Add(this.outError);
            this.gbOut.Controls.Add(lblOut5);
            this.gbOut.Controls.Add(this.outAngle);
            this.gbOut.Controls.Add(lblOut4);
            this.gbOut.Controls.Add(this.outGyro);
            this.gbOut.Controls.Add(lblOut2);
            this.gbOut.Controls.Add(this.outAccelAngle);
            this.gbOut.Controls.Add(lblOut1);
            this.gbOut.Location = new System.Drawing.Point(13, 564);
            this.gbOut.Name = "gbOut";
            this.gbOut.Size = new System.Drawing.Size(530, 106);
            this.gbOut.TabIndex = 8;
            this.gbOut.TabStop = false;
            this.gbOut.Text = "Data";
            // 
            // outBattery
            // 
            this.outBattery.Location = new System.Drawing.Point(77, 69);
            this.outBattery.Name = "outBattery";
            this.outBattery.ReadOnly = true;
            this.outBattery.Size = new System.Drawing.Size(84, 20);
            this.outBattery.TabIndex = 5;
            this.outBattery.TabStop = false;
            // 
            // outM2
            // 
            this.outM2.Location = new System.Drawing.Point(415, 69);
            this.outM2.Name = "outM2";
            this.outM2.ReadOnly = true;
            this.outM2.Size = new System.Drawing.Size(84, 20);
            this.outM2.TabIndex = 17;
            this.outM2.TabStop = false;
            // 
            // outM1
            // 
            this.outM1.Location = new System.Drawing.Point(415, 43);
            this.outM1.Name = "outM1";
            this.outM1.ReadOnly = true;
            this.outM1.Size = new System.Drawing.Size(84, 20);
            this.outM1.TabIndex = 15;
            this.outM1.TabStop = false;
            // 
            // outOutput
            // 
            this.outOutput.Location = new System.Drawing.Point(415, 17);
            this.outOutput.Name = "outOutput";
            this.outOutput.ReadOnly = true;
            this.outOutput.Size = new System.Drawing.Size(84, 20);
            this.outOutput.TabIndex = 13;
            this.outOutput.TabStop = false;
            // 
            // outInt
            // 
            this.outInt.Location = new System.Drawing.Point(244, 69);
            this.outInt.Name = "outInt";
            this.outInt.ReadOnly = true;
            this.outInt.Size = new System.Drawing.Size(84, 20);
            this.outInt.TabIndex = 11;
            this.outInt.TabStop = false;
            // 
            // outError
            // 
            this.outError.Location = new System.Drawing.Point(244, 43);
            this.outError.Name = "outError";
            this.outError.ReadOnly = true;
            this.outError.Size = new System.Drawing.Size(84, 20);
            this.outError.TabIndex = 9;
            this.outError.TabStop = false;
            // 
            // outAngle
            // 
            this.outAngle.Location = new System.Drawing.Point(244, 17);
            this.outAngle.Name = "outAngle";
            this.outAngle.ReadOnly = true;
            this.outAngle.Size = new System.Drawing.Size(84, 20);
            this.outAngle.TabIndex = 7;
            this.outAngle.TabStop = false;
            // 
            // outGyro
            // 
            this.outGyro.Location = new System.Drawing.Point(77, 43);
            this.outGyro.Name = "outGyro";
            this.outGyro.ReadOnly = true;
            this.outGyro.Size = new System.Drawing.Size(84, 20);
            this.outGyro.TabIndex = 3;
            this.outGyro.TabStop = false;
            // 
            // outAccelAngle
            // 
            this.outAccelAngle.Location = new System.Drawing.Point(77, 17);
            this.outAccelAngle.Name = "outAccelAngle";
            this.outAccelAngle.ReadOnly = true;
            this.outAccelAngle.Size = new System.Drawing.Size(84, 20);
            this.outAccelAngle.TabIndex = 1;
            this.outAccelAngle.TabStop = false;
            // 
            // serialPortSelector
            // 
            this.serialPortSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serialPortSelector.BaudRate = 115200;
            this.serialPortSelector.Location = new System.Drawing.Point(350, 12);
            this.serialPortSelector.Name = "serialPortSelector";
            this.serialPortSelector.Size = new System.Drawing.Size(193, 110);
            this.serialPortSelector.TabIndex = 3;
            this.serialPortSelector.ConnectRequest += new System.EventHandler(this.serialPortSelector_ConnectRequest);
            this.serialPortSelector.DisconnectRequest += new System.EventHandler(this.serialPortSelector_DisconnectRequest);
            // 
            // batteryRequestTimer
            // 
            this.batteryRequestTimer.Interval = 5000;
            this.batteryRequestTimer.Tick += new System.EventHandler(this.batteryRequestTimer_Tick);
            // 
            // TunerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 680);
            this.Controls.Add(this.gbOut);
            this.Controls.Add(gbOther);
            this.Controls.Add(gbMotor);
            this.Controls.Add(gbFilter);
            this.Controls.Add(this.serialPortSelector);
            this.Controls.Add(gbAutoSend);
            this.Controls.Add(gbI);
            this.Controls.Add(gbD);
            this.Controls.Add(gbP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TunerForm";
            this.Text = "SEGWAY";
            this.Load += new System.EventHandler(this.TunerForm_Load);
            gbP.ResumeLayout(false);
            gbP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbP)).EndInit();
            gbD.ResumeLayout(false);
            gbD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbD)).EndInit();
            gbI.ResumeLayout(false);
            gbI.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbI)).EndInit();
            gbAutoSend.ResumeLayout(false);
            gbAutoSend.PerformLayout();
            gbFilter.ResumeLayout(false);
            gbFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHigh)).EndInit();
            gbMotor.ResumeLayout(false);
            gbMotor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudM2Boost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudM1Boost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDead)).EndInit();
            gbOther.ResumeLayout(false);
            gbOther.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAccelOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIntLimit)).EndInit();
            this.gbOut.ResumeLayout(false);
            this.gbOut.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.Ports.SerialPort serial;
        private System.Windows.Forms.TrackBar tbP;
        private System.Windows.Forms.ComboBox cbMinP;
        private System.Windows.Forms.NumericUpDown nudP;
        private System.Windows.Forms.ComboBox cbMaxP;
        private System.Windows.Forms.ComboBox cbMinD;
        private System.Windows.Forms.NumericUpDown nudD;
        private System.Windows.Forms.ComboBox cbMaxD;
        private System.Windows.Forms.TrackBar tbD;
        private System.Windows.Forms.ComboBox cbMinI;
        private System.Windows.Forms.NumericUpDown nudI;
        private System.Windows.Forms.ComboBox cbMaxI;
        private System.Windows.Forms.TrackBar tbI;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.CheckBox cbAutoSend;
        private Comm.SerialPortSelector serialPortSelector;
        private System.Windows.Forms.NumericUpDown nudLow;
        private System.Windows.Forms.NumericUpDown nudHigh;
        private System.Windows.Forms.CheckBox cbFilterLinked;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.NumericUpDown nudRound;
        private System.Windows.Forms.NumericUpDown nudDead;
        private System.Windows.Forms.Timer timoutTimer;
        private System.Windows.Forms.Timer rerequestTimer;
        private System.Windows.Forms.NumericUpDown nudIntLimit;
        private System.Windows.Forms.NumericUpDown nudAccelOffset;
        private System.Windows.Forms.GroupBox gbOut;
        private System.Windows.Forms.TextBox outAccelAngle;
        private System.Windows.Forms.TextBox outBattery;
        private System.Windows.Forms.TextBox outM2;
        private System.Windows.Forms.TextBox outM1;
        private System.Windows.Forms.TextBox outOutput;
        private System.Windows.Forms.TextBox outInt;
        private System.Windows.Forms.TextBox outError;
        private System.Windows.Forms.TextBox outAngle;
        private System.Windows.Forms.TextBox outGyro;
        private System.Windows.Forms.NumericUpDown nudM2Boost;
        private System.Windows.Forms.NumericUpDown nudM1Boost;
        private System.Windows.Forms.Timer batteryRequestTimer;

    }
}

