﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WiimoteLib;

namespace CapstoneSegway.PID_Tuner
{
    public partial class DriveForm : Form
    {
        public EventHandler<DriveEventArgs> DriveCommand;
        private Int32 hMargin = 80, vMargin = 50;
        private Int32 size = 500;
        private Rectangle activeArea;
        private System.Drawing.Point center;
        private Int32 dotX, dotY;
        private Wiimote balanceBoard = null;
        private Int32 bbXOffset = 0, bbYOffset = 0;
        private long lastUpdate = 0;

        public DriveForm() {
            InitializeComponent();
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
        }

        private void DriveForm_Load(object sender, EventArgs e) {
            activeArea = new Rectangle(hMargin, vMargin, size, size);
            center = new System.Drawing.Point(hMargin + size / 2, vMargin + size / 2);
            ClientSize = new Size(2 * hMargin + size, 2 * vMargin + size);
            UpdateCommand(center.X, center.Y);
        }

        private void DriveForm_FormClosing(object sender, FormClosingEventArgs e) {
            DisconnectWiimote();
        }

        private void Application_ApplicationExit(object sender, EventArgs e) {
            DisconnectWiimote();
        }

        private void DriveForm_Paint(object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;
            g.Clear(this.BackColor);
            g.DrawLine(Pens.Black, activeArea.Left, center.Y, activeArea.Right, center.Y);
            g.DrawLine(Pens.Black, center.X, activeArea.Top, center.X, activeArea.Bottom);
            g.DrawRectangle(Pens.Black, activeArea);
            g.FillEllipse(Brushes.Red, dotX - 5, dotY - 5, 10, 10);
        }

        private void nudMinAngle_ValueChanged(object sender, EventArgs e) {
            NumericUpDown current = (NumericUpDown)sender;
            NumericUpDown other = null;
            if (current == nudMaxAngle) other = nudMinAngle;
            else if (current == nudMaxSteer) other = nudMinSteer;
            else return;

            other.Value = -current.Value;
        }

        private void DriveForm_MouseClick(object sender, MouseEventArgs e) {
            if (!cbBalance.Checked && activeArea.Contains(e.Location)) {
                UpdateCommand(e.X, e.Y);
            }
        }

        private void btnZero_Click(object sender, EventArgs e) {
            bbXOffset += dotX - center.X;
            bbYOffset += dotY - center.Y;
        }

        private void cbBalance_CheckedChanged(object sender, EventArgs e) {
            if (!cbBalance.Checked) {
                DisconnectWiimote();
                return;
            }
            using (new HourGlass()) {
                if (ConnectWiimote()) {
                    btnZero.Enabled = true;
                    UpdateCommand(center.X, center.Y);
                } else {
                    cbBalance.Checked = false;
                }
            }
        }

        private Boolean ConnectWiimote() {
            WiimoteCollection wiimotes = new WiimoteCollection();
            try {
                wiimotes.FindAllWiimotes();
            } catch (WiimoteNotFoundException ex) {
                MessageBox.Show(
                    "Wiimote not found:" + Environment.NewLine + ex.Message,
                    "Wiimote connection error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                cbBalance.Checked = false;
                return false;
            } catch (WiimoteException ex) {
                MessageBox.Show(
                    "Wiimote error:" + Environment.NewLine + ex.Message,
                    "Wiimote connection error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                cbBalance.Checked = false;
                return false;
            }

            foreach (Wiimote wm in wiimotes) {
                wm.Connect();
                if (wm.WiimoteState.ExtensionType != ExtensionType.BalanceBoard) {
                    wm.Disconnect();
                    continue;
                }
                balanceBoard = wm;
                wm.SetLEDs(true, false, false, false);
                balanceBoard.WiimoteChanged += new EventHandler<WiimoteChangedEventArgs>(OnWiimoteChanged);
                return true;
            }

            MessageBox.Show(
                "Wiimote error:" + Environment.NewLine + "Not a balance board",
                "Wiimote connection error",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
            return false;
        }

        private void DisconnectWiimote() {
            if (balanceBoard != null) {
                using (new HourGlass()) {
                    try {
                        balanceBoard.SetLEDs(false, false, false, false);
                    } catch (Exception) {
                        // ignore
                    }
                    balanceBoard.Disconnect();
                    balanceBoard = null;
                }
            }
            btnZero.Enabled = false;
            UpdateCommand(center.X, center.Y);
        }

        private void OnWiimoteChanged(object sender, WiimoteChangedEventArgs e) {
            if (InvokeRequired) {
                BeginInvoke((MethodInvoker)delegate() { OnWiimoteChanged(sender, e); });
                return;
            }

            if (e.WiimoteState.ExtensionType != ExtensionType.BalanceBoard || !cbBalance.Checked) return;

            float x = center.X;
            float y = center.Y;
            if (e.WiimoteState.BalanceBoardState.WeightLb > 50) {
                x += e.WiimoteState.BalanceBoardState.CenterOfGravity.X * (float)nudXScale.Value;
                y += e.WiimoteState.BalanceBoardState.CenterOfGravity.Y * (float)nudYScale.Value;
                x -= bbXOffset;
                y -= bbYOffset;
            }

            // Only send the actual update events every 75 ms
            long now = DateTime.Now.Ticks;
            if (now - lastUpdate > 10000 * 75) {
                lastUpdate = now;
                UpdateCommand((int)x, (int)y);
            }
        }

        private void UpdateCommand(int x, int y) {
            if (y < activeArea.Top) y = activeArea.Top;
            if (y > activeArea.Bottom) y = activeArea.Bottom;
            if (x < activeArea.Left) x = activeArea.Left;
            if (x > activeArea.Right) x = activeArea.Right;

            float angleProportion = (float)(y - activeArea.Top) / activeArea.Height;
            float angle = (float)nudMaxAngle.Value - (angleProportion * (float)(nudMaxAngle.Value - nudMinAngle.Value));
            float steerProportion = (float)(x - activeArea.Left) / activeArea.Width;
            Int32 steer = (Int32)((float)nudMinSteer.Value + (steerProportion * (float)(nudMaxSteer.Value - nudMinSteer.Value)));
            lblAngle.Text = String.Format((String)lblAngle.Tag, angle);
            lblSteer.Text = String.Format((String)lblSteer.Tag, steer);

            dotX = x;
            dotY = y;
            this.Invalidate();

            EventHandler<DriveEventArgs> handler = DriveCommand;
            if (handler != null) handler(null, new DriveEventArgs(angle, steer));
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams {
            get {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
    }

    public class DriveEventArgs : EventArgs
    {
        public DriveEventArgs(float angle, Int32 steer) {
            Angle = angle;
            Steering = steer;
        }
        public float Angle { get; private set; }
        public Int32 Steering { get; private set; }
    }
}
