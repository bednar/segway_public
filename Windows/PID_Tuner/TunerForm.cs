﻿using System;
using System.Drawing;
using System.IO;
using System.Collections.Concurrent;
using System.Windows.Forms;
using CapstoneSegway.Comm;

namespace CapstoneSegway.PID_Tuner
{
    public partial class TunerForm : Form
    {
        NumericUpDown[] kNuds = new NumericUpDown[3];
        TrackBar[] kTbs = new TrackBar[3];
        ComboBox[] kCbMaxs = new ComboBox[3];
        ComboBox[] kCbMins = new ComboBox[3];
        private FrameDecoder frameDecoder;
        private MessageDecoder messageDecoder;
        private ConcurrentDictionary<ParameterItem, Int32> unreceivedParameters;
        private DateTime lastRx;
        private GraphForm graph;
        private float pOutputComponent, dOutputComponent, iOutputComponent;
        private float pidOutput;
        private float estAngle;
        private float targetAngle = 0;
        private DriveForm remote;

        public TunerForm() {
            InitializeComponent();

            frameDecoder = new FrameDecoder();
            frameDecoder.FrameReceived += new EventHandler(OnFrameReceived);
            messageDecoder = new MessageDecoder();
            messageDecoder.ParameterReportReceived += new EventHandler<MessageReceivedEventArgs>(OnParameterReportReceived);
            messageDecoder.TelemetryReportReceived += new EventHandler<MessageReceivedEventArgs>(OnTelemetryReportReceived);

            unreceivedParameters = new ConcurrentDictionary<ParameterItem, int>(2, 16);

            SetupPidUI();
            nudP.Tag = ParameterItem.Kp;
            nudD.Tag = ParameterItem.Kd;
            nudI.Tag = ParameterItem.Ki;
            nudLow.Tag = ParameterItem.LowpassCoeffient;
            nudHigh.Tag = ParameterItem.HighpassCoeffient;
            nudDead.Tag = ParameterItem.MotorDeadZone;
            nudRound.Tag = ParameterItem.MotorRoundUp;
            nudM1Boost.Tag = ParameterItem.M1Boost;
            nudM2Boost.Tag = ParameterItem.M2Boost;
            nudIntLimit.Tag = ParameterItem.MaxErrorIntegral;
            nudAccelOffset.Tag = ParameterItem.AccelOffset;

            graph = new GraphForm();
            remote = new DriveForm();
            remote.DriveCommand += new EventHandler<DriveEventArgs>(OnDriveCommand);
        }

        #region Serial Connection Management

        private void serialPortSelector_ConnectRequest(object sender, EventArgs e) {
            using (new HourGlass()) {
                serial.PortName = serialPortSelector.PortName;
                serial.BaudRate = serialPortSelector.BaudRate;
                try {
                    serial.Open();
                    lastRx = DateTime.Now;
                    batteryRequestTimer_Tick(sender, EventArgs.Empty);
                } catch (Exception ex) {
                    MessageBox.Show(
                        ex.GetType().ToString() + Environment.NewLine + ex.Message,
                        "Connection Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                } finally {
                    UpdateConnectionState();
                }
            }
        }

        private void serialPortSelector_DisconnectRequest(object sender, EventArgs e) {
            using (new HourGlass()) {
                StopParameterRequest();
                try {
                    serial.Close();
                } catch (Exception ex) {
                    MessageBox.Show(
                        ex.GetType().ToString() + Environment.NewLine + ex.Message,
                        "Disconnection Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                } finally {
                    UpdateConnectionState();
                }
            }
        }

        private void SafeDisconnect() {
            if (this.InvokeRequired) {
                this.BeginInvoke(new MethodInvoker(SafeDisconnect));
            } else {
                serialPortSelector_DisconnectRequest(null, EventArgs.Empty);
            }
        }

        private void timoutTimer_Tick(object sender, EventArgs e) {
            if (!serialPortSelector.Connected) {
                SafeDisconnect();
                return;
            }
            if (lastRx.AddSeconds(3) < DateTime.Now) {
                SafeDisconnect();
                MessageBox.Show(
                    "Receive timeout: No data received from the Segway for 3 seconds",
                    "Disconnected",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                );
            }
        }

        private void UpdateConnectionState() {
            btnSend.Enabled = btnLoad.Enabled = serialPortSelector.Connected = batteryRequestTimer.Enabled = timoutTimer.Enabled = serial.IsOpen;
        }

        #endregion

        #region Parameter UI

        #region PID Constants

        private void SetupPidUI() {
            kNuds = new NumericUpDown[] { nudP, nudD, nudI };
            kTbs = new TrackBar[] { tbP, tbD, tbI };
            kCbMaxs = new ComboBox[] { cbMaxP, cbMaxD, cbMaxI };
            kCbMins = new ComboBox[] { cbMinP, cbMinD, cbMinI };

            for (Int32 i = 0; i < kNuds.Length; i++) {
                kCbMins[i].SelectedItem = kNuds[i].Minimum.ToString();
                kCbMaxs[i].SelectedItem = kNuds[i].Maximum.ToString();
            }
            RefreshSliders();
        }

        private void cbMin_SelectionChangeCommitted(object sender, EventArgs e) {
            Int32 i = (Int32)((Control)sender).Tag;

            // Don't set min above (or equal to) the max
            Decimal v = Decimal.Parse(kCbMins[i].SelectedItem.ToString());
            if (v >= kNuds[i].Maximum) {
                kCbMins[i].SelectedItem = kNuds[i].Minimum.ToString();
                return;
            }
            kNuds[i].Minimum = v;


            // Move the current value up if smaller than min
            if (kNuds[i].Value < v) {
                kNuds[i].Value = v;
            }

            // Redraw
            RefreshSliders();
        }

        private void cbMax_SelectionChangeCommitted(object sender, EventArgs e) {
            Int32 i = (Int32)((Control)sender).Tag;

            // Don't set max below (or equal to) the min
            Decimal v = Decimal.Parse(kCbMaxs[i].SelectedItem.ToString());
            if (v <= kNuds[i].Minimum) {
                kCbMaxs[i].SelectedItem = kNuds[i].Maximum.ToString();
                return;
            }
            kNuds[i].Maximum = v;


            // Move the current value down if large than max
            if (kNuds[i].Value > v) {
                kNuds[i].Value = v;
            }

            // Redraw
            RefreshSliders();
        }

        private void nudK_ValueChanged(object sender, EventArgs e) {
            Int32 i = (Int32)(ParameterItem)((Control)sender).Tag - (Int32)ParameterItem.Kp;
            RefreshSlider(i);
            SendParameters(false, (NumericUpDown)sender);
        }

        private void tb_ValueChanged(object sender, EventArgs e) {
            Int32 i = (Int32)((Control)sender).Tag;
            Decimal min = kNuds[i].Minimum, max = kNuds[i].Maximum;
            kNuds[i].Value = min + (kTbs[i].Value * (max - min) / 100);
        }

        private void RefreshSliders() {
            for (Int32 i = 0; i < kTbs.Length; i++) {
                RefreshSlider(i);
            }
        }

        private void RefreshSlider(Int32 i) {
            TrackBar slider = kTbs[i];
            Decimal min = kNuds[i].Minimum, cur = kNuds[i].Value, max = kNuds[i].Maximum;
            slider.Value = (Int32)((cur - min) / (max - min) * 100);
        }

        private void ExpandKBounds(Int32 i, Decimal value) {
            ComboBox low = kCbMins[i], high = kCbMaxs[i];

            Boolean changedItem = false;
            if (value < Decimal.Parse(low.SelectedItem.ToString())) {
                for (Int32 j = 0; j < low.Items.Count; j++) {
                    if (value > Decimal.Parse(low.Items[j].ToString())) {
                        low.SelectedIndex = j;
                        changedItem = true;
                        break;
                    }
                }
                if (!changedItem) low.SelectedIndex = low.Items.Count - 1;
                cbMin_SelectionChangeCommitted(low, new EventArgs());
            }

            changedItem = false;
            if (value > Decimal.Parse(high.SelectedItem.ToString())) {
                for (Int32 j = high.Items.Count - 1; j >= 0; j--) {
                    if (value < Decimal.Parse(high.Items[j].ToString())) {
                        high.SelectedIndex = j;
                        changedItem = true;
                        break;
                    }
                }
                if (!changedItem) high.SelectedIndex = 0;
                cbMax_SelectionChangeCommitted(high, new EventArgs());
            }
        }

        #endregion

        private void nudFilters_ValueChanged(object sender, EventArgs e) {
            NumericUpDown current = (NumericUpDown)sender;
            if (!cbFilterLinked.Checked) {
                SendParameters(false, current);
                return;
            }
            if (current.Value > 1) {
                current.Value = 1;
            }
            NumericUpDown other = (current == nudLow ? nudHigh : nudLow);
            other.Value = 1 - current.Value;
            SendParameters(false, current);
        }

        private void cbFilterLinked_CheckedChanged(object sender, EventArgs e) {
            if (nudHigh.Value > 1) nudHigh.Value = 1;
            nudLow.Value = 1 - nudHigh.Value;
        }

        private void nud_ValueChanged(object sender, EventArgs e) {
            SendParameters(false, (NumericUpDown)sender);
        }

        #endregion

        #region RX

        private void OnFrameReceived(object sender, EventArgs e) {
            this.pOutputComponent = this.dOutputComponent = this.iOutputComponent = float.NaN;
            this.pidOutput = float.NaN;
            this.estAngle = float.NaN;
            try {
                // This raises any the TelemetryReportReceived event handler, which sets the floats we NaNed
                messageDecoder.ProcessMessages(frameDecoder.LastPayload);
            } catch (InvalidDataException) {
                // ignore
                return;
            }

            // Save the data to prevent a race condition
            float p = this.pOutputComponent, d = this.dOutputComponent, i = this.iOutputComponent;
            float pid = this.pidOutput;
            if (!float.IsNaN(p) && !float.IsNaN(d) && !float.IsNaN(i) && !float.IsNaN(pid)) {
                this.BeginInvoke((MethodInvoker)delegate() {
                    graph.AddComponentPoints(p, d, i, pid);
                });
            }
            float angle = this.estAngle, target = this.targetAngle;
            if (!float.IsNaN(angle)) {
                this.BeginInvoke((MethodInvoker)delegate() {
                    graph.AddAnglePoints(angle, target);
                });
            }
        }

        private void OnParameterReportReceived(object sender, MessageReceivedEventArgs e) {
            ParameterItem item = (ParameterItem)e.MessageCode;
            Decimal value;
            if (e.IsFloat) value = (Decimal)e.FloatValue;
            else if (e.IsInt16) value = (Decimal)e.Int16Value;
            else if (e.IsUInt16) value = (Decimal)e.UInt16Value;
            else throw new NotImplementedException("This data type is not yet supported");

            SafeSetParameterUI(item, value);

            Int32 dummy;
            unreceivedParameters.TryRemove(item, out dummy);
        }

        private void OnTelemetryReportReceived(object sender, MessageReceivedEventArgs e) {
            TelemetryItem item = (TelemetryItem)e.MessageCode;
            String value;
            if (e.IsFloat) value = e.FloatValue.ToString();
            else if (e.IsInt16) value = e.Int16Value.ToString();
            else if (e.IsUInt16) value = e.UInt16Value.ToString();
            else throw new NotImplementedException("This data type is not yet supported");

            SafeSetTelemetryUI(item, value);

            switch (item) {
                case TelemetryItem.EstimatedErrorDegrees:
                    pOutputComponent = e.FloatValue * (float)nudP.Value;
                    break;
                case TelemetryItem.GyroThetaDotDPS:
                    dOutputComponent = e.FloatValue * (float)nudD.Value;
                    break;
                case TelemetryItem.ErrorIntergral:
                    iOutputComponent = e.FloatValue * (float)nudI.Value;
                    break;
                case TelemetryItem.PidOutput:
                    pidOutput = (float)e.Int16Value;
                    break;
                case TelemetryItem.EstimatedThetaDegrees:
                    estAngle = e.FloatValue;
                    break;
            }
        }

        private void rerequestTimer_Tick(object sender, EventArgs e) {
            if (unreceivedParameters.IsEmpty) {
                StopParameterRequest();

                // Restore filter linked checkbox
                if (nudLow.Value + nudHigh.Value == 1) {
                    cbFilterLinked.Checked = (Boolean)cbFilterLinked.Tag;
                }
                cbFilterLinked.Tag = null;
            } else {
                RequestParameters();
            }
        }

        private void SafeSetParameterUI(ParameterItem item, Decimal value) {
            if (this.InvokeRequired) {
                this.BeginInvoke((MethodInvoker)delegate { SafeSetParameterUI(item, value); });
                return;
            }

            NumericUpDown nud = null;
            switch (item) {
                case ParameterItem.MotorDeadZone:
                    nud = nudDead;
                    break;
                case ParameterItem.MotorRoundUp:
                    nud = nudRound;
                    break;
                case ParameterItem.M1Boost:
                    nud = nudM1Boost;
                    break;
                case ParameterItem.M2Boost:
                    nud = nudM2Boost;
                    break;
                case ParameterItem.SteerAmount:
                    // TODO: steering
                    break;
                case ParameterItem.Kp:
                    nud = nudP;
                    ExpandKBounds(0, value);
                    break;
                case ParameterItem.Kd:
                    nud = nudD;
                    ExpandKBounds(1, value);
                    break;
                case ParameterItem.Ki:
                    ExpandKBounds(2, value);
                    nud = nudI;
                    break;
                case ParameterItem.HighpassCoeffient:
                    nud = nudHigh;
                    break;
                case ParameterItem.LowpassCoeffient:
                    nud = nudLow;
                    break;
                case ParameterItem.MaxErrorIntegral:
                    nud = nudIntLimit;
                    break;
                case ParameterItem.AccelOffset:
                    nud = nudAccelOffset;
                    break;
                case ParameterItem.TargetAngle:
                    // TODO: display target angle in UI
                    targetAngle = (float)value;
                    break;
            }
            if (nud == null) {
                throw new NotImplementedException(String.Format("Setting parameter {0} is not currently supported", item));
            }

            try {
                nud.Value = value;
            } catch (ArgumentOutOfRangeException) {
                MessageBox.Show(
                    String.Format("Invalid value {0} received for {1}", value, item),
                    "Load parameters",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
            }
        }

        private void RequestParameters() {
            Byte[] buffer = new Byte[FrameDecoder.MaxRawFrameSize];
            Int32 len = 0;
            foreach (var item in unreceivedParameters.Keys) {
                len += Comm.Message.CreateParameterValueRequest(buffer, len, item);
            }
            rerequestTimer.Enabled = true;
            SendMessage(buffer, len);
        }

        private void StopParameterRequest() {
            this.UseWaitCursor = false;
            rerequestTimer.Enabled = false;

            Int32 dummy;
            foreach (var k in unreceivedParameters.Keys) {
                unreceivedParameters.TryRemove(k, out dummy);
            }
        }

        private void SafeSetTelemetryUI(TelemetryItem item, String value) {
            if (this.InvokeRequired) {
                this.BeginInvoke((MethodInvoker)delegate { SafeSetTelemetryUI(item, value); });
                return;
            }

            TextBox tb = null;
            switch (item) {
                case TelemetryItem.M1Command:
                    tb = outM1;
                    break;
                case TelemetryItem.M2Command:
                    tb = outM2;
                    break;
                case TelemetryItem.PidOutput:
                    tb = outOutput;
                    break;
                case TelemetryItem.AccelerometerThetaDegrees:
                    tb = outAccelAngle;
                    break;
                case TelemetryItem.GyroThetaDotDPS:
                    tb = outGyro;
                    break;
                case TelemetryItem.EstimatedThetaDegrees:
                    tb = outAngle;
                    break;
                case TelemetryItem.EstimatedErrorDegrees:
                    tb = outError;
                    break;
                case TelemetryItem.ErrorIntergral:
                    tb = outInt;
                    break;
                case TelemetryItem.BatteryVolts:
                    tb = outBattery;
                    float b = 0;
                    float.TryParse(value, out b);
                    tb.BackColor = (b < 11.0) ? Color.Red : ((b < 11.4) ? Color.Yellow : ((b < 12.1) ? SystemColors.Control : Color.LimeGreen));
                    break;
            }
            if (tb == null) return;
            tb.Text = value;
        }

        #endregion

        private void OnDriveCommand(object sender, DriveEventArgs e) {
            if (!serial.IsOpen) return;

            Byte[] buffer = new Byte[FrameDecoder.MaxRawFrameSize];
            Int32 len = 0;
            len += Comm.Message.CreateParameterSetMessage(buffer, len, ParameterItem.SteerAmount, (Int16)e.Steering);
            len += Comm.Message.CreateParameterSetMessage(buffer, len, ParameterItem.TargetAngle, e.Angle);
            SendMessage(buffer, len);
            targetAngle = e.Angle;
        }

        private void SendParameters(Boolean force, params NumericUpDown[] controls) {
            if (!force && !cbAutoSend.Checked) return;
            if (!serial.IsOpen) return;

            foreach (NumericUpDown nud in controls) {
                ParameterItem item = (ParameterItem)nud.Tag;
                Byte[] message = new Byte[FrameDecoder.MaxRawFrameSize];
                Int32 len;
                Type t = Comm.Message.GetParameterDataType(item);
                if (t == typeof(UInt16)) len = Comm.Message.CreateParameterSetMessage(message, 0, item, (UInt16)nud.Value);
                else if (t == typeof(Int16)) len = Comm.Message.CreateParameterSetMessage(message, 0, item, (Int16)nud.Value);
                else if (t == typeof(float)) len = Comm.Message.CreateParameterSetMessage(message, 0, item, (float)nud.Value);
                else throw new NotImplementedException("Data type not supported");
                SendMessage(message, len);
            }
        }

        private void SendMessage(Byte[] message, Int32 length) {
            byte[] frameBuffer = new Byte[FrameDecoder.MaxRawFrameSize];
            length = FrameDecoder.CreateFrame(message, length, frameBuffer);
            try {
                serial.Write(new Byte[] { 0 }, 0, 1);
                serial.Write(frameBuffer, 0, length);
                serial.Write(new Byte[] { 0 }, 0, 1);
            } catch (Exception ex) {
                if (ex is InvalidOperationException || ex is TimeoutException) {
                    MessageBox.Show(
                        "Error while writing data:" + Environment.NewLine + ex.Message,
                        "Write Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                } else {
                    throw;
                }
            }
        }

        private void btnSend_Click(object sender, EventArgs e) {
            // TODO: autodectect nuds
            SendParameters(true, nudP, nudD, nudI, nudLow, nudHigh, nudDead, nudRound, nudM1Boost, nudM2Boost, nudIntLimit, nudAccelOffset);
        }

        private void btnLoad_Click(object sender, EventArgs e) {
            foreach (Control c in this.Controls) {
                if (c is GroupBox) {
                    foreach (Control d in c.Controls) {
                        if (d is NumericUpDown) {
                            unreceivedParameters.TryAdd((ParameterItem)d.Tag, 0);
                        }
                    }
                }
            }
            this.UseWaitCursor = true;
            cbFilterLinked.Tag = cbFilterLinked.Checked;
            cbFilterLinked.Checked = false;
            RequestParameters();
        }

        private void batteryRequestTimer_Tick(object sender, EventArgs e) {
            Byte[] buffer = new Byte[FrameDecoder.MaxRawFrameSize];
            Int32 len = 0;
            len += Comm.Message.CreateTelemetryReportRequest(buffer, len, TelemetryItem.BatteryVolts);
            SendMessage(buffer, len);
        }

        private void serial_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e) {
            lastRx = DateTime.Now;

            Byte b;
            try {
                while (serial.BytesToRead != 0) {
                    b = (Byte)serial.ReadByte();
                    frameDecoder.ConsumeByte(b);
                }
            } catch (InvalidOperationException ex) {
                MessageBox.Show(
                    "Error while reading data" + Environment.NewLine + ex.Message,
                    "Read Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                SafeDisconnect();
            }
        }

        private void TunerForm_Load(object sender, EventArgs e) {
            graph.Show();
            remote.Show();
        }
    }
}
