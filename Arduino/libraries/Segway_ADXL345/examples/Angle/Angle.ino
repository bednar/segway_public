/**
 * AXDL345 Accelerometer Library - Angle Demo
 * Displays the angle between the negative x-axis and vertical in degrees.
 **/

#include <Wire.h>
#include <Segway_ADXL345.h>

Segway_ADXL345 accel;
double arcsin_table[256];

void setup()
{
  Wire.begin();
  Serial.begin(115200);
  if (!accel.begin()) {
    Serial.println("Can't connect to accelerometer");
    while(1);
  }
  delay(150);

  accel.setRange(accel.ADXL345_RANGE_4_G);
  accel.setDataRate(accel.ADXL345_DATARATE_800_HZ);
  accel.setXOffset(1.1);
  accel.setYOffset(0.3);
  accel.setZOffset(1.7);

  for(int i = 0; i < 256; i++) {
    arcsin_table[i] = asin((double(i)-128.0) / 128.0) * 180/PI;
  }
}

void loop()
{
  accel.poll();
  double z = accel.getZ();
  double x = accel.getX();

  double tilt_scaled = -z / 9.81;
  // double tilt_scaled= -z / sqrt(pow(x,2) + pow(z,2)); // account for motion (total accel != 1G)

  // Theta is the angle from vertical--forward is positive
  // This is an approximation of:
  // double theta = asin(tilt_scaled) * 180/PI;
  int index = (tilt_scaled+1)*127 + 1;
  double theta = arcsin_table[constrain(index, 0, 255)];

  Serial.println("Angle: ");
  Serial.println(theta);

  delay(100);
}
