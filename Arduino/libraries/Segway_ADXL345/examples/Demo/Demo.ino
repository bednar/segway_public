/**
 * AXDL345 Accelerometer Library - Demo
 * Displays the acceleration along each axis and the net acceleration.
 **/

#include <Wire.h>
#include <Segway_ADXL345.h>

Segway_ADXL345 accel;

void setup()
{
  Wire.begin();
  Serial.begin(57000);
  if (!accel.begin()) {
    Serial.println("Can't connect to accelerometer");
    while(1);
  }
  delay(150);

  accel.setRange(accel.ADXL345_RANGE_4_G);
  accel.setDataRate(accel.ADXL345_DATARATE_50_HZ);
  accel.setXOffset(1.1);
  accel.setYOffset(0.3);
  accel.setZOffset(1.7);
}

void loop()
{
  accel.poll();

  float net = sqrt(pow(accel.getX(),2) + pow(accel.getY(),2) + pow(accel.getZ(),2));

  Serial.print("x:  ");
  Serial.print(accel.getX());
  Serial.print("\ty:  ");
  Serial.print(accel.getY());
  Serial.print("\tz:  ");
  Serial.print(accel.getZ());
  Serial.print("\tnet:  ");
  Serial.println(net);

  delay(100);
}
