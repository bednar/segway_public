/**
 * AXDL345 Accelerometer Library - Calibration
 *
 * Calibration procedure:
 * 1) Run sketch and enter zero for the offset
 * 2) Place the x-axis parallel to the ground so that it should read zero
 * 3) The value shown for the x acceleration is your offset. Rerun the sketch
 *    using this value instead. The x-value should now be close to zero.
 * 4) Uncomment the x line at end of the setup function and enter your value.
 * 5) Change the two lines marked "edit here" to use the y-axis and repeat
 *    steps 1 to 4 using the y-axis instead.
 * 6) Repeat for the z-axis.
 **/

#include <Wire.h>
#include <Segway_ADXL345.h>

Segway_ADXL345 accel;

void setup()
{
  Wire.begin();
  Serial.begin(57000);
  if (!accel.begin()) {
    Serial.println("Can't connect to accelerometer");
    while(1);
  }
  delay(150);

  accel.setRange(accel.ADXL345_RANGE_4_G);
  accel.setDataRate(accel.ADXL345_DATARATE_50_HZ);
  // accel.setXOffset(1.1);
  // accel.setYOffset(0.3);
  // accel.setYOffset(1.7);
}

void loop()
{
  Serial.println("Enter offset:");
  while (Serial.available() == 0);
  delay(10);
  accel.setXOffset(Serial.parseFloat()); // edit here
  while (Serial.available() != 0) {
    Serial.read();
  }
  delay(10);
  Serial.print("Set offset: ");
  Serial.println(accel.getXOffset()); // edit here

  delay(50);

  int i = 0;
  for (i = 0; i < 10; i++) {
    accel.poll();

    float net = sqrt(pow(accel.getX(),2) + pow(accel.getY(),2) + pow(accel.getZ(),2));

    Serial.print("x:  ");
    Serial.print(accel.getX());
    Serial.print("\ty:  ");
    Serial.print(accel.getY());
    Serial.print("\tz:  ");
    Serial.print(accel.getZ());
    Serial.print("\tnet:  ");
    Serial.println(net);

    delay(100);
  }
  delay(250);
}
