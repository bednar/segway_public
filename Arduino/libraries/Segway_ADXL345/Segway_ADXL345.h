/**
 * Team Segway
 * AXDL345 Accelerometer Library
 *
 * Based on the Adafruit_ADS1015 library, see license.txt
 **/

#ifndef __SEGWAY_ADXL345_H
#define __SEGWAY_ADXL345_H

#include <inttypes.h>


class Segway_ADXL345 {
 public:
  // Used with register 0x2C (ADXL345_REG_BW_RATE) to set bandwidth
  enum dataRate_t {
    ADXL345_DATARATE_3200_HZ    = 0b1111, // 1600Hz Bandwidth
    ADXL345_DATARATE_1600_HZ    = 0b1110, //  800Hz Bandwidth
    ADXL345_DATARATE_800_HZ     = 0b1101, //  400Hz Bandwidth
    ADXL345_DATARATE_400_HZ     = 0b1100, //  200Hz Bandwidth
    ADXL345_DATARATE_200_HZ     = 0b1011, //  100Hz Bandwidth
    ADXL345_DATARATE_100_HZ     = 0b1010, //   50Hz Bandwidth
    ADXL345_DATARATE_50_HZ      = 0b1001, //   25Hz Bandwidth
    ADXL345_DATARATE_25_HZ      = 0b1000, // 12.5Hz Bandwidth
    ADXL345_DATARATE_12_5_HZ    = 0b0111, // 6.25Hz Bandwidth
    ADXL345_DATARATE_6_25HZ     = 0b0110, // 3.13Hz Bandwidth
    ADXL345_DATARATE_3_13_HZ    = 0b0101, // 1.56Hz Bandwidth
    ADXL345_DATARATE_1_56_HZ    = 0b0100, // 0.78Hz Bandwidth
    ADXL345_DATARATE_0_78_HZ    = 0b0011, // 0.39Hz Bandwidth
    ADXL345_DATARATE_0_39_HZ    = 0b0010, // 0.20Hz Bandwidth
    ADXL345_DATARATE_0_20_HZ    = 0b0001, // 0.10Hz Bandwidth
    ADXL345_DATARATE_0_10_HZ    = 0b0000  // 0.05Hz Bandwidth (default value)
  };

  // Used with register 0x31 (ADXL345_REG_DATA_FORMAT) to set g range */
  enum range_t {
    ADXL345_RANGE_16_G          = 0b11,   // +/- 16g
    ADXL345_RANGE_8_G           = 0b10,   // +/- 8g
    ADXL345_RANGE_4_G           = 0b01,   // +/- 4g
    ADXL345_RANGE_2_G           = 0b00    // +/- 2g (default value)
  };


  Segway_ADXL345();

  bool begin();
  void poll();
  float getX();
  float getY();
  float getZ();

  void setRange(range_t);
  range_t getRange();
  void setDataRate(dataRate_t);
  dataRate_t getDataRate();

  void setXOffset(float); // in m/s, +- 2G, resolution is ~16 mg
  void setYOffset(float);
  void setZOffset(float);
  float getXOffset();
  float getYOffset();
  float getZOffset();


 protected:
  uint8_t readByte(uint8_t reg);
  void writeByte(uint8_t reg, uint8_t value);
  float x, y, z;
  int16_t rawX, rawY, rawZ;
};


/* ========== OTHER CONSTANTS ========== */
#define ADXL345_ADDRESS             (0x53)     // Assumes ALT address pin low
#define ADXL345_MG2G_MULTIPLIER     (0.0039)   // nominal 4mg per lsb, typ 3.9
#define ADXL345_DEVID               (0xE5)     // fixed value of ADXL345_REG_DEVID
#define GRAVITY_SI                  (9.80665F)


/* ========== REGISTERS ========== */
#define ADXL345_REG_DEVID           (0x00)  // Device ID
#define ADXL345_REG_THRESH_TAP      (0x1D)  // Tap threshold
#define ADXL345_REG_OFSX            (0x1E)  // X-axis offset
#define ADXL345_REG_OFSY            (0x1F)  // Y-axis offset
#define ADXL345_REG_OFSZ            (0x20)  // Z-axis offset
#define ADXL345_REG_DUR             (0x21)  // Tap duration
#define ADXL345_REG_LATENT          (0x22)  // Tap latency
#define ADXL345_REG_WINDOW          (0x23)  // Tap window
#define ADXL345_REG_THRESH_ACT      (0x24)  // Activity threshold
#define ADXL345_REG_THRESH_INACT    (0x25)  // Inactivity threshold
#define ADXL345_REG_TIME_INACT      (0x26)  // Inactivity time
#define ADXL345_REG_ACT_INACT_CTL   (0x27)  // Axis enable control for activity and inactivity detection
#define ADXL345_REG_THRESH_FF       (0x28)  // Free-fall threshold
#define ADXL345_REG_TIME_FF         (0x29)  // Free-fall time
#define ADXL345_REG_TAP_AXES        (0x2A)  // Axis control for single/double tap
#define ADXL345_REG_ACT_TAP_STATUS  (0x2B)  // Source for single/double tap
#define ADXL345_REG_BW_RATE         (0x2C)  // Data rate and power mode control
#define ADXL345_REG_POWER_CTL       (0x2D)  // Power-saving features control
#define ADXL345_REG_INT_ENABLE      (0x2E)  // Interrupt enable control
#define ADXL345_REG_INT_MAP         (0x2F)  // Interrupt mapping control
#define ADXL345_REG_INT_SOURCE      (0x30)  // Source of interrupts
#define ADXL345_REG_DATA_FORMAT     (0x31)  // Data format control
#define ADXL345_REG_DATAX0          (0x32)  // X-axis data 0
#define ADXL345_REG_DATAX1          (0x33)  // X-axis data 1
#define ADXL345_REG_DATAY0          (0x34)  // Y-axis data 0
#define ADXL345_REG_DATAY1          (0x35)  // Y-axis data 1
#define ADXL345_REG_DATAZ0          (0x36)  // Z-axis data 0
#define ADXL345_REG_DATAZ1          (0x37)  // Z-axis data 1
#define ADXL345_REG_FIFO_CTL        (0x38)  // FIFO control
#define ADXL345_REG_FIFO_STATUS     (0x39)  // FIFO status


#endif
