/**
 * Team Segway
 * AXDL345 Accelerometer Library
 *
 * Based on the Adafruit_ADS1015 library, see license.txt
 **/

#include <Arduino.h>
#include <Wire.h>
#include <math.h>

#include "Segway_ADXL345.h"

Segway_ADXL345::Segway_ADXL345() {

}

bool Segway_ADXL345::begin() {
  if (readByte(ADXL345_REG_DEVID) != ADXL345_DEVID) {
    return false;
  }

  writeByte(ADXL345_REG_POWER_CTL, 0x08); // awake in measure mode
  return true;
}

void Segway_ADXL345::poll() {
  Wire.beginTransmission(ADXL345_ADDRESS);
  Wire.write(ADXL345_REG_DATAX0);
  Wire.endTransmission();
  Wire.requestFrom(ADXL345_ADDRESS, 6);
  rawX = (int16_t) Wire.read() | (Wire.read() << 8);
  rawY = (int16_t) Wire.read() | (Wire.read() << 8);
  rawZ = (int16_t) Wire.read() | (Wire.read() << 8);
  x = rawX * ADXL345_MG2G_MULTIPLIER * GRAVITY_SI;
  y = rawY * ADXL345_MG2G_MULTIPLIER * GRAVITY_SI;
  z = rawZ * ADXL345_MG2G_MULTIPLIER * GRAVITY_SI;
}

float Segway_ADXL345::getX() {
  return x;
}

float Segway_ADXL345::getY() {
  return y;
}

float Segway_ADXL345::getZ() {
  return z;
}

void Segway_ADXL345::setRange(range_t range) {
  // Red the data format register to preserve bits
  uint8_t format = readByte(ADXL345_REG_DATA_FORMAT);

  // Update the data rate
  format &= ~0x0F; // clear FULL_RES, Justify, and range
  format |= range;

  // Make sure that the FULL-RES bit is enabled for range scaling
  format |= (1<<3);

  // Write the register back to the IC
  writeByte(ADXL345_REG_DATA_FORMAT, format);
}

Segway_ADXL345::range_t Segway_ADXL345::getRange() {
  return (Segway_ADXL345::range_t) (readByte(ADXL345_REG_DATA_FORMAT) & 0b0011);
}

void Segway_ADXL345::setDataRate(Segway_ADXL345::dataRate_t rate) {
  // leave LOW_POWER at 0 (off - regular operation)
  writeByte(ADXL345_REG_BW_RATE, rate & 0b1111);
}

Segway_ADXL345::dataRate_t Segway_ADXL345::getDataRate() {
  return (Segway_ADXL345::dataRate_t) (readByte(ADXL345_REG_BW_RATE) & 0b1111);
}

static uint8_t siToOffset(float si) {
  // Serial.print(si);
  // Serial.print(" m/s^2 -> ");
  // Serial.print((uint8_t) round(si / GRAVITY_SI / ADXL345_MG2G_MULTIPLIER / 4));
  // Serial.println(" LSB");
  return (uint8_t) round(si / GRAVITY_SI / ADXL345_MG2G_MULTIPLIER / 4);
}

static float offsetToSI(uint8_t offset) {
  // Serial.print(offset);
  // Serial.print(" LSB -> ");
  // Serial.print((float)(offset * ADXL345_MG2G_MULTIPLIER * 4 * GRAVITY_SI));
  // Serial.println(" m/s^2");
  return offset * ADXL345_MG2G_MULTIPLIER * 4 * GRAVITY_SI;
}

void Segway_ADXL345::setXOffset(float offset) {
  writeByte(ADXL345_REG_OFSX, siToOffset(offset));
}

void Segway_ADXL345::setYOffset(float offset) {
  writeByte(ADXL345_REG_OFSY, siToOffset(offset));
}

void Segway_ADXL345::setZOffset(float offset) {
  writeByte(ADXL345_REG_OFSZ, siToOffset(offset));
}

float Segway_ADXL345::getXOffset() {
  return offsetToSI(readByte(ADXL345_REG_OFSX));
}

float Segway_ADXL345::getYOffset() {
  return offsetToSI(readByte(ADXL345_REG_OFSY));
}

float Segway_ADXL345::getZOffset() {
  return offsetToSI(readByte(ADXL345_REG_OFSZ));
}

uint8_t Segway_ADXL345::readByte(uint8_t reg) {
  Wire.beginTransmission(ADXL345_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(ADXL345_ADDRESS, 1);
  return Wire.read();
}

void Segway_ADXL345::writeByte(uint8_t reg, uint8_t value) {
  Wire.beginTransmission(ADXL345_ADDRESS);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}