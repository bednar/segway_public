/**
 * Team Segway
 * Communication Link Library
 *
 * Frame format:
 *  - Frames are delimited by one or more zero bytes before and after each frame.
 *  - An unencoded frame consists of a 1 to 62 byte payload followed by a 1 byte CRC.
 *  - The entire frame is byte stuffed using COBS to remove zero bytes. The resulting
 *    raw frame is 3 to 64 bytes in length.
 *  - The CRC used is the Dallas/Maxim iButton CRC-8
 *
 * COBS implementation based off:
 * http://www.jacquesf.com/2011/03/consistent-overhead-byte-stuffing/
 * See this paper for information about COBS:
 * http://conferences.sigcomm.org/sigcomm/1997/papers/p062.pdf
 *
 * Implementation Note: The rx payload buffer actually holds the entire raw frame until
 * after read returns true.
 **/

// #define DEBUG_SEGWAY_COMM_FRAME_RX
// #define DEBUG_SEGWAY_COMM_MSG_RX

#include "Comm.h"
#include <util/crc16.h>

Segway_Comm::Segway_Comm(Stream &s) :
  rxPayloadLength(0),
  onParameterRequest(NULL),
  onSetParameter(NULL),
  onTelemetryRequest(NULL),
  s(s),
  synced(false),
  hadValidFrame(false)
{ }

/// Read available data until exhausted or a full (valid or invalid) frame
/// is found.
/// Returns: true if a valid frame is available in the buffer
bool Segway_Comm::read() {
  while (s.available()) {
    uint8_t b = s.read();
    if (synced) {
      // Reset if the last call to read returned true--we don't need
      // another zero to start this frame
      if (hadValidFrame) {
        rxPayloadLength = 0;
        hadValidFrame = false;
      }

      if (b == 0) { // found end of frame
        return decodeAndValidateOrDestroyFrame();
      }
      else if (rxPayloadLength == MAX_RAW_FRAME_SIZE) { // frame too long
        #ifdef DEBUG_SEGWAY_COMM_FRAME_RX
        s.println("Frame too long");
        #endif
        synced = false;
        return false;
      }
      else { // frame data
        rxPayload[rxPayloadLength++] = b;
      }
    }
    else if (b == 0) { // not synced, found start of frame
      synced = true;
      hadValidFrame = false;
      rxPayloadLength = 0;
      #ifdef DEBUG_SEGWAY_COMM_FRAME_RX
      s.println("Synced");
      #endif
    }
    else { // not synced, not the frame delimiter
      // drop the byte
      #ifdef DEBUG_SEGWAY_COMM_FRAME_RX
      s.print("Drop unsync: ");
      s.println(b, HEX);
      #endif
    }
  }
  return false;
}

/// Encodes a frame and writes it to s.
/// Returns: true unless data length is invalid
bool Segway_Comm::write(const uint8_t *data, size_t length, bool streaming) {
  if (length < 1 || length > MAX_RAW_FRAME_SIZE-2) {
    return false;
  }

  if (!streaming) {
    s.write((uint8_t)0x00);
  }
  uint8_t crc = calculateCRC(data, length);

  // COBS encode (and write)
  // We can't use the function without making two extra buffers--one to hold the payload + crc and
  // another to hold the encoded output.
  size_t readIndex = 0;
  size_t startOfBlock = 0;
  uint8_t code = 1;
  size_t i;
  // Encoded and write the data except for the final block
  while (readIndex < length) {
    if (data[readIndex] == 0) {
      // Finish the block that this zero byte ends
      s.write(code);
      for (i = startOfBlock; i < readIndex; i++) {
        s.write(data[i]);
      }

      // Start a new block
      code = 1;
      startOfBlock = ++readIndex;
    }
    else {
      code++;
      readIndex++;
    }
  }
  // Process the CRC byte
  if (crc == 0) {
    // Finish the final data block with the CRC zero
    s.write(code);
    for (i = startOfBlock; i < readIndex; i++) {
      s.write(data[i]);
    }
    // Start and finish the final block (the implicit zero)
    code = 1;
    s.write(code);
  }
  else {
    // Finish the final data block / final block
    code++;
    s.write(code);
    for (i = startOfBlock; i < readIndex; i++) {
      s.write(data[i]);
    }
    s.write(crc);
  }

  s.write((uint8_t)0x00);
  return true;
}

/// Checks length of frame, decodes, and checks CRC
/// If valid, rxPayloadLength is adjusted to the length of the actual data
/// If invalid, the frame is destroyed by setting rxPayloadLength to 0
bool Segway_Comm::decodeAndValidateOrDestroyFrame() {
  // Check 3 byte min length: 1 byte cobs overhead + 1 data byte + 1 byte CRC
  if (rxPayloadLength < 3) {
    #ifdef DEBUG_SEGWAY_COMM_FRAME_RX
    s.print("Dropped short frame: ");
    for (size_t i = 0; i < rxPayloadLength; i++) {
      s.print(rxPayload[i], HEX);
      s.print(" ");
    }
    s.println("");
    #endif
    rxPayloadLength = 0;
    hadValidFrame = false;
    return false;
  }

  // Decode and verify no errors
  size_t decodedLength = cobsDecode(rxPayload, rxPayloadLength, rxPayload);
  if (decodedLength != --rxPayloadLength) {
    // most likely invalid cobs data
    #ifdef DEBUG_SEGWAY_COMM_FRAME_RX
    s.print("Dropped frame for bad encoding: ");
    for (size_t i = 0; i < rxPayloadLength+1; i++) {
      s.print(rxPayload[i], HEX);
      s.print(" ");
    }
    s.println("");
    #endif
    rxPayloadLength = 0;
    hadValidFrame = false;
    return false;
  }

  // Check that the CRC of rxPayload[0..rxPayloadLength-2] is rxPayload[rxPayloadLength-1]
  if (calculateCRC(rxPayload, rxPayloadLength-1) != rxPayload[rxPayloadLength-1]) {
    #ifdef DEBUG_SEGWAY_COMM_FRAME_RX
    s.print("Dropped frame for bad CRC: (decoded) ");
    for (size_t i = 0; i < rxPayloadLength; i++) {
      s.print(rxPayload[i], HEX);
      s.print(" ");
    }
    s.println("");
    #endif
    rxPayloadLength = 0;
    hadValidFrame = false;
    return false;
  }
  rxPayloadLength--; // no more crc
  hadValidFrame = true;
  #ifdef DEBUG_SEGWAY_COMM_FRAME_RX
  s.print("Got valid frame: (payload only)");
  for (size_t i = 0; i < rxPayloadLength; i++) {
    s.print(rxPayload[i], HEX);
    s.print(" ");
  }
  s.println("");
  #endif
  return true;
}

bool Segway_Comm::handleFrame() {
  size_t offset = 0;
  while (offset < rxPayloadLength) {
    // Find the proper number of bytes associated with the message
    uint8_t code = rxPayload[offset++];
    size_t dataLen;
    if (code < MSG_SET_PARAM) dataLen = 0;
    else if (code < MSG_SET_PARAM + MSG_FLOAT) dataLen = 2;
    else if (code < MSG_REQUEST_TELEMETRY) dataLen = 4;
    else if (code < MSG_RESERVED) dataLen = 0;
    else {
      #ifdef DEBUG_SEGWAY_COMM_MSG_RX
      s.print("Received unknown message type code ");
      s.print(code, HEX);
      s.println("; dropping remainder of frame");
      #endif
      return false;
    }
    if (offset + dataLen > rxPayloadLength) {
      #ifdef DEBUG_SEGWAY_COMM_MSG_RX
      s.println("Received incomplete message; dropping remainder of frame");
      #endif
      return false;
    }

    // Dispatch the message
    message_callback handler = NULL;
    if (code < MSG_SET_PARAM) {
      if (onParameterRequest != NULL) onParameterRequest(code);
    }
    else if (code < MSG_REQUEST_TELEMETRY) {
      if (onSetParameter != NULL) onSetParameter(code - MSG_SET_PARAM, rxPayload + offset, dataLen);
    }
    else {
      if (onTelemetryRequest != NULL) onTelemetryRequest(code);
    }

    offset += dataLen;
  }
}

/// Calculates a CRC using the Dallas/Maxim iButton CRC-8
uint8_t Segway_Comm::calculateCRC(const uint8_t *data, size_t length) {
  uint8_t crc = 0;
  for (size_t i = 0; i < length; i++) {
    crc = _crc_ibutton_update(crc, data[i]);
  }
  return crc;
}

/// COBS encodes input[0..length-1] into output[0..length]
/// Input lengths of 0 to 253 bytes are supported
/// The output is 1 byte longer than the input
/// Returns: length of output or 0 if input is too long
/*
size_t Segway_Comm::cobsEncode(const uint8_t *input, size_t length, uint8_t *output) {
  size_t readIndex = 0;
  size_t writeIndex = 1;
  size_t codeIndex = 0;
  uint8_t code = 1;

  if (length > 253) {
    return 0;
  }

  while (readIndex < length) {
    if (input[readIndex] == 0) {
      // Finish the block that this zero byte ends
      output[codeIndex] = code;

      // Start a new block
      code = 1;
      codeIndex = writeIndex++;
      readIndex++;
    }
    else {
      // Copy the non-zero byte
      output[writeIndex++] = input[readIndex++];
      code++;
    }
  }

  output[codeIndex] = code;

  return writeIndex;
}
*/

/// COBS decodes input[0..length] into output[0..length-1]
/// Input lengths of 1 to 254 bytes are supported
/// The output is 1 byte shorter than the input
/// This operation may be performed in place (i.e. input == output)
/// Returns: length of output or 0 if invalid input data or length
size_t Segway_Comm::cobsDecode(const uint8_t *input, size_t length, uint8_t *output) {
  if (length == 0 || length > 254) {
    return 0;
  }

  size_t readIndex = 0;
  size_t writeIndex = 0;
  uint8_t code;

  while (readIndex < length) {
    // Start a block
    code = input[readIndex];

    if (code == 0) {
      return 0; // No zeros are allowed
    }
    if (readIndex + code > length && code != 1) {
      return 0; // code takes us out of source array
    }

    // Copy the block's data bytes
    readIndex++;
    for (uint8_t i = 1; i < code; i++) { // note 1 based
      if (input[readIndex] == 0) {
        return 0; // No zeros are allowed
      }
      output[writeIndex++] = input[readIndex++];
    }

    // Finish the block (but suppress the implicit zero at the end)
    if (readIndex != length) {
      output[writeIndex++] = 0;
    }
  }
  return writeIndex; // number of bytes written
}
