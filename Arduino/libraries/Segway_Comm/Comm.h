/**
 * Team Segway
 * Communication Link Library
 *
 * Frame format:
 *  - Frames are delimited by one or more zero bytes before and after each frame.
 *  - An unencoded frame consists of a 1 to 62 byte payload followed by a 1 byte CRC.
 *  - The entire frame is byte stuffed using COBS to remove zero bytes. The resulting
 *    raw frame is 3 to 64 bytes in length.
 *  - The CRC used is the Dallas/Maxim iButton CRC-8
 *
 * COBS implementation based off:
 * http://www.jacquesf.com/2011/03/consistent-overhead-byte-stuffing/
 * See this paper for information about COBS:
 * http://conferences.sigcomm.org/sigcomm/1997/papers/p062.pdf
 **/

#ifndef Comm_h
#define Comm_h

#include <inttypes.h>
#include <stream.h>

#define MAX_RAW_FRAME_SIZE  64

#define MSG_UINT16             0x00
#define MSG_INT16              0x10
#define MSG_FLOAT              0x20
// Incoming
#define MSG_REQUEST_PARAM      0x00
#define MSG_SET_PARAM          0x30
#define MSG_REQUEST_TELEMETRY  0x60
#define MSG_RESERVED           0x90

typedef void (*data_message_callback)(uint8_t, const uint8_t*, size_t);
typedef void (*message_callback)(uint8_t);

class Segway_Comm {
public:
  Segway_Comm(Stream &s);
  bool read();
  bool write(const uint8_t *data, size_t length, bool streaming=true);
  bool handleFrame();
  uint8_t rxPayload[MAX_RAW_FRAME_SIZE];
  size_t rxPayloadLength;
  message_callback onParameterRequest;
  data_message_callback onSetParameter;
  message_callback onTelemetryRequest;
private:
  Stream &s;
  bool synced, hadValidFrame;
  bool decodeAndValidateOrDestroyFrame();
  uint8_t calculateCRC(const uint8_t *data, size_t length);
  // size_t cobsEncode(const uint8_t * input, size_t length, uint8_t * output);
  size_t cobsDecode(const uint8_t * input, size_t length, uint8_t * output);
};

#endif
