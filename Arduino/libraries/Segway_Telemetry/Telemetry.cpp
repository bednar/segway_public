/**
 * Team Segway
 * Telemetry Library - A framework for regular data reporting
 *
 * Items in group 0 are never sent as a group. Its items are only transmitted
 * when individually requested with writeItem. Items within each other group
 * are sent together in a single frame when writeGroup is called, with only one
 * group being transmitted per call to writeGroup.
 *
 * To set up:
 *  1) Register non-auto items
 *  2) Call beginFirstGroup
 *  3) Register a group of auto items
 *  4) Register additional groups of auto items, calling beginGroup before each
 **/

#include "Telemetry.h"

Segway_Telemetry::Segway_Telemetry(Segway_Comm &comm) :
  comm(comm),
  itemCount(0),
  firstAutoItem(0),
  currentItem(0),
  groupCount(1),
  currentGroup(0)
{ }

void Segway_Telemetry::registerDataItem(uint16_t &var, uint8_t item) {
  registerDataItem(&var, TYPE_UINT16, item);
}

void Segway_Telemetry::registerDataItem(int16_t &var, uint8_t item) {
  registerDataItem(&var, TYPE_INT16, item);
}

void Segway_Telemetry::registerDataItem(float &var, uint8_t item) {
  registerDataItem(&var, TYPE_FLOAT, item);
}

void Segway_Telemetry::registerDataItem(uint16_callback func, uint8_t item) {
  // It's not legal C++ to covert a function pointer into a data pointer but
  // it works on this platform with this toolchain
  registerDataItem((void*)func, TYPE_UINT16_CALLBACK, item);
}

void Segway_Telemetry::registerDataItem(float_callback func, uint8_t item) {
  // It's not legal C++ to covert a function pointer into a data pointer but
  // it works on this platform with this toolchain
  registerDataItem((void*)func, TYPE_FLOAT_CALLBACK, item);
}

void Segway_Telemetry::beginFirstGroup() {
  currentGroup = 1;
  groupCount = 2;
  currentItem = firstAutoItem = itemCount;
}

void Segway_Telemetry::beginGroup() {
  groupCount++;
}

bool Segway_Telemetry::writeGroup() {
  uint8_t txBuffer[MAX_RAW_FRAME_SIZE];
  size_t offset = 0;
  while (currentItem < itemCount) {
    if (itemGroups[currentItem] == currentGroup) {
      offset += createMessage(txBuffer + offset, currentItem);
      currentItem++;
    } else {
      break;
    }
  }
  currentGroup++;
  if (currentItem >= itemCount) {
    currentItem = firstAutoItem;
  }
  if (currentGroup >= groupCount) {
    currentGroup = 1;
  }
  return comm.write(txBuffer, offset);
}

bool Segway_Telemetry::writeItem(uint8_t item) {
  uint8_t txBuffer[MAX_RAW_FRAME_SIZE];
  size_t length = 0;
  for (uint8_t i = 0; i < itemCount; i++) {
    if (items[i] == item) {
      length = createMessage(txBuffer, i);
      return comm.write(txBuffer, length, false);
    }
  }
  return false;
}

void Segway_Telemetry::registerDataItem(void *var, Type type, uint8_t item) {
  itemPointers[itemCount] = var;
  itemTypes[itemCount] = type;
  items[itemCount] = item;
  itemGroups[itemCount] = groupCount - 1;
  itemCount++;
}

size_t Segway_Telemetry::createMessage(uint8_t *buffer, uint8_t index) {
  buffer[0] = items[index];

  // Create a pointer to the first byte of the data
  uint8_t *ptr = (uint8_t*) itemPointers[index];
  unsigned int iData;
  float fData;
  if (itemTypes[index] == TYPE_UINT16_CALLBACK) {
    iData = ((uint16_callback)itemPointers[index])();
    ptr = (uint8_t*) &iData;
  }
  else if (itemTypes[index] == TYPE_FLOAT_CALLBACK) {
    fData = ((float_callback)itemPointers[index])();
    ptr = (uint8_t*) &fData;
  }

  // Pack the appropriate number of bytes into the buffer in network order
  switch (itemTypes[index]) {
  case TYPE_UINT16:
  case TYPE_INT16:
  case TYPE_UINT16_CALLBACK:
    buffer[1] = ptr[1];
    buffer[2] = ptr[0];
    return 3;
  case TYPE_FLOAT:
  case TYPE_FLOAT_CALLBACK:
    buffer[1] = ptr[3];
    buffer[2] = ptr[2];
    buffer[3] = ptr[1];
    buffer[4] = ptr[0];
    return 5;
  }

  return 1; // Just the code
}
