#######################################
# Datatypes (KEYWORD1)
#######################################
Segway_Telemetry	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################
registerDataItem	KEYWORD2
beginFirstGroup	KEYWORD2
beginGroup	KEYWORD2
writeGroup	KEYWORD2
writeItem	KEYWORD2

#######################################
# Instances (KEYWORD2)
#######################################

#######################################
# Constants (LITERAL1)
#######################################
