/**
 * Team Segway
 * Telemetry Library - A framework for regular data reporting
 *
 * Items in group 0 are never sent as a group. Its items are only transmitted
 * when individually requested with writeItem. Items within each other group
 * are sent together in a single frame when writeGroup is called, with only one
 * group being transmitted per call to writeGroup.
 *
 * To set up:
 *  1) Register non-auto items
 *  2) Call beginFirstGroup
 *  3) Register a group of auto items
 *  4) Register additional groups of auto items, calling beginGroup before each
 **/

#ifndef Telemtry_h
#define Telemtry_h

#include <inttypes.h>
#include <Comm.h>

#define TELEMETRY_MAX_ITEMS (32)
typedef unsigned int (*uint16_callback)();
typedef float (*float_callback)();

class Segway_Telemetry {
public:
  Segway_Telemetry(Segway_Comm &comm);
  void registerDataItem(uint16_t &var, uint8_t item);
  void registerDataItem(int16_t &var, uint8_t item);
  void registerDataItem(float &var, uint8_t item);
  void registerDataItem(uint16_callback func, uint8_t item);
  void registerDataItem(float_callback func, uint8_t item);
  void beginFirstGroup();
  void beginGroup();
  bool writeGroup();
  bool writeItem(uint8_t item);
private:
  enum Type {
    TYPE_UINT16 = 0,
    TYPE_INT16 = 1,
    TYPE_FLOAT = 2,
    TYPE_UINT16_CALLBACK = 3,
    TYPE_FLOAT_CALLBACK = 4
  };
  Segway_Comm &comm;
  uint8_t items[TELEMETRY_MAX_ITEMS];
  void* itemPointers[TELEMETRY_MAX_ITEMS];
  uint8_t itemTypes[TELEMETRY_MAX_ITEMS];
  uint8_t itemGroups[TELEMETRY_MAX_ITEMS];
  uint8_t itemCount;
  uint8_t firstAutoItem;
  uint8_t currentItem;
  uint8_t groupCount;
  uint8_t currentGroup;
  void registerDataItem(void *var, Type type, uint8_t item);
  size_t createMessage(uint8_t *buffer, uint8_t index);
};

#endif
