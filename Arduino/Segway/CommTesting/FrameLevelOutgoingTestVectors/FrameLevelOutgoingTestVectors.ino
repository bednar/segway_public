/**
 * Test vectors to check the RX code on Windows / Android at the **frame level**
 *
 * Frames are delimitered by one or more null bytes. They consist of 1 to 62 payload
 * bytes followed by a 1 byte CRC-8 (iButton). The entire frame is COBS encoded to
 * remove nulls, with a fixed 1 byte of overhead. This results in a 3 to 64 byte frame.
 **/

void setup() {
  Serial.begin(115200);

  // Should be dropped
  Serial.print("ZYX");

  // Frame AA-BB with CRC CC (encodeing error) and delayed end of frame
  Serial.write(0);
  Serial.print("\xAA\xBB\xCC");
  delay(2000);
  Serial.write(0);

  // Frame 05-AA-BB-CC with bad CRC FF
  delay(1000);
  Serial.print("\x05\xAA\xBB\xCC\xFF");
  Serial.write(0);

  // Frame 05-AA-BB-CC with good CRC D4
  delay(1000);
  Serial.print("\x05\xAA\xBB\xCC\xD4");
  Serial.write(0);

  // Max length frame with delay before \0
  // 1 Encoding overhead + 62 data + 1 CRC
  delay(2000);
  Serial.print("\x40");
  Serial.print("\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F");
  Serial.print("\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F");
  Serial.print("\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2A\x2B\x2C\x2D\x2E\x2F");
  Serial.print("\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\x3E");
  Serial.print("\x3C");
  delay(1000);
  Serial.write(0);

  // 1 byte too long
  // 1 Encoding overhead + 63 data + 1 CRC
  delay(2000);
  Serial.print("\x41");
  Serial.print("\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F");
  Serial.print("\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F");
  Serial.print("\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2A\x2B\x2C\x2D\x2E\x2F");
  Serial.print("\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3A\x3B\x3C\x3D\x3E\x3F");
  delay(1000);
  Serial.print("\xE2"); // Should drop entire RX buffer and cause desync
  Serial.write(0); // Should resync

  // Too short
  Serial.print("\x01");
  Serial.write(0);

  // Too short
  Serial.print("\x01\x01");
  Serial.write(0);

  // Minimum frame: 1 byte data 11 and valid checksum C3
  Serial.print("\x03\x11\xC3");
  Serial.write(0);
}

void loop() {
  delay(10000);
}


