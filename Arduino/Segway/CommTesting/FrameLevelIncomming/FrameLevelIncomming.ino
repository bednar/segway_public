/**
 * A simple test of the Arduino frame RX code
 *
 * Binary frame data is read from Serial and **text** diagnostic messages
 * are sent out over the same Serial port. The other end needs to send
 * frames but receive text!
 *
 * For details you must uncomment the #define DEBUG_SEGWAY_COMM_FRAME_RX line
 * in Comm.cpp
 **/

#include <Comm.h>

Segway_Comm Comm(Serial);

void setup() {
  Serial.begin(115200);
}

void loop() {
  if (Comm.read()){ 
    Serial.println("Handle frame");
  }
}

