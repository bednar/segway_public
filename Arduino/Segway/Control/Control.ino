/********************************************************
 * Main Control for the big boy Segway
 ********************************************************/

#include <Wire.h>
#include <Segway_ADXL345.h>
#include <L3G.h>
#include <DualVNH5019MotorShield.h>
#include <Comm.h>
#include <Telemetry.h>
#include "Motors.h"

#define BAUD 115200

Segway_Comm Comm = Segway_Comm(Serial);
Segway_Telemetry Telemetry = Segway_Telemetry(Comm);
Segway_ADXL345 accel;
L3G gyro;

const int potPin = A2;
const int pushButtonPin = 3;
const int killSwitchPin = 5;
const int batterySensePin = A3;

boolean toggleFlag = false;

const float gyro_scale = 70;

// Parameters
float targetAngle = 0;
int steerAmount = 0;
float highpassCoeff = 0.995, lowpassCoeff = 0.005;
float Kp=200, Kd=2.5, Ki=1000;
float maxErrorInt = 0.4;
int motorDeadZone = 20, motorRoundUp = 50;
int m1Boost = 15, m2Boost = 0;
float accelOffset = 0; // degrees

// Inputs / state / outputs
float thetaA, thetaDotG;
float estTheta, estError, errorInt;
int output, outputM1, outputM2;

void setup() {
  Serial.begin(BAUD);
  Wire.begin();
  Motors.begin();

  // Set up sensors
  if (!accel.begin() || !gyro.init()) {
    Serial.println("Can't connect to sensors");
    while (1);
  }
  // gyro.enableDefault();
  // default is 0x0F
  // default + 800 Hz Data Output Rate is 0xCF
  gyro.writeReg(L3G_CTRL_REG1, 0xCF);
  accel.setRange(accel.ADXL345_RANGE_4_G);
  accel.setDataRate(accel.ADXL345_DATARATE_800_HZ);
  accel.setXOffset(1.1);
  accel.setYOffset(0.3);
  accel.setZOffset(1.7);

  // Set up pins
  pinMode(pushButtonPin, INPUT_PULLUP);
  pinMode(killSwitchPin, INPUT_PULLUP);

  // Initialize control variables
  accel.poll();
  estTheta = thetaA = asinLUT(-accel.getZ() / 9.81) - accelOffset;
  estError = estTheta - targetAngle;
  errorInt = 0;

  setupTelemetry();
}

// unsigned long time = 0; // loop timing.  440 Hz without transmit, 380 Hz with transmit
void loop() {
  // Toggle a pin every loop to allow measurement of controller frequency (2 loops / pin cycle)
  toggleFlag ? (digitalWrite(13, HIGH)) : (digitalWrite(13, LOW));
  toggleFlag = !toggleFlag;

  //Serial.println(micros() - time);
  //time = micros();
  const float dt = 0.00227; // (440 Hz)^-1

  // Read from sensors
  accel.poll();
  gyro.read();
  thetaDotG = -gyro.g.y / gyro_scale;
  thetaA = asinLUT(-accel.getZ() / 9.81) - accelOffset;
  //thetaA = asin(-accel.getZ() / 9.81) - accelOffset;

  // Calculate new theta/error estimates
  float dThetaG = thetaDotG * dt; // integrate gyro reading
  estTheta = highpassCoeff * (estTheta + dThetaG); // high-pass gyro
  estTheta += lowpassCoeff * thetaA; // low-pass accelerometer
  estError = estTheta - targetAngle;

  // Compute & limit the error integral
  errorInt += estError * dt;
  if (errorInt > maxErrorInt) {
    errorInt = maxErrorInt;
  }
  else if (errorInt < -maxErrorInt) {
    errorInt = -maxErrorInt;
  }

  // Calculate PID output
  output = Kp*estError + Kd*thetaDotG + Ki*errorInt;
  output = constrain(output, -400, 400); // Is this needed?  <<--- No, we just left it there for clarity.  I am ok with remvoing it -- ZL

  // Perform steering
  outputM1 = output - steerAmount;
  outputM2 = output + steerAmount;

  // Control the motors
  if (digitalRead(killSwitchPin) == LOW) {
    outputM1 = outputM2 = 0;
    Motors.setBrakes(400, 400);
  }
  else {
    if ((outputM1 > -motorDeadZone) && (outputM1 < motorDeadZone)) {
      Motors.setM1Brake(400);
      outputM1 = 0;
    }
    else if ((outputM1 > -motorRoundUp) && (outputM1 < 0)) {
      outputM1 = -motorRoundUp - m1Boost;
      outputM1 = constrain(outputM1, -400, 400);
      Motors.setM1Speed(outputM1);
    }
    else if ((outputM1 < motorRoundUp) && (outputM1 > 0)) {
      outputM1 = motorRoundUp + m1Boost;
      outputM1 = constrain(outputM1, -400, 400);
      Motors.setM1Speed(outputM1);
    }
    else {
      outputM1 += (outputM1 >= 0) ? m1Boost : -m1Boost;
      outputM1 = constrain(outputM1, -400, 400);
      Motors.setM1Speed(outputM1);
    }
    if ((outputM2 > -motorDeadZone) && (outputM2 < motorDeadZone)) {
      Motors.setM2Brake(400);
      outputM2 = 0;
    }
    else if ((outputM2 > -motorRoundUp) && (outputM2 < 0)) {
      outputM2 = -motorRoundUp - m2Boost;
      outputM2 = constrain(outputM2, -400, 400);
      Motors.setM2Speed(outputM2);
    }
    else if ((outputM2 < motorRoundUp) && (outputM2 > 0)) {
      outputM2 = motorRoundUp + m2Boost;
      outputM2 = constrain(outputM2, -400, 400);
      Motors.setM2Speed(outputM2);
    }
    else {
      outputM2 += (outputM2 >= 0) ? m2Boost : -m2Boost;
      outputM2 = constrain(outputM2, -400, 400);
      Motors.setM2Speed(outputM2);
    }
  }

  // Wired remote command
  if (digitalRead(pushButtonPin) == LOW) {
    // targetAngle = (analogRead(potPin) - 512.0) / 512.0;
    Telemetry.writeGroup();
  }

  if (Comm.read()) {
    Comm.handleFrame();
  }
  // Telemetry.writeGroup();
  Serial.write(0); // Send an empty frame as a keep-alive while normal transmit is commented out
  Motors.faultCheck();
}

