/**
 * Motor support for main control program
 **/

#ifndef Motors_h
#define Motors_h

#include "DualVNH5019MotorShield.h"

class SegwayMotors {
public:
  SegwayMotors();
  void begin();
  void setSpeeds(int m1Speed, int m2Speed);
  void setM1Speed(int speed);
  void setM2Speed(int speed);
  void setBrakes(int m1Brake, int m2Brake);
  void setM1Brake(int brake);
  void setM2Brake(int brake);
  void kill();
  void faultCheck();
  unsigned int getM1CurrentMilliamps();
  unsigned int getM2CurrentMilliamps();

private:
  DualVNH5019MotorShield md;
  boolean stopFlag;
};

extern SegwayMotors Motors;

#endif
