/**
 * Communication / telemetry section of main control program
 **/

enum ParameterCode {
  /* 0x00-0x0F -- uint16 */
  /* 0x10-0x1F -- int16 */
  PARAM_MOTOR_DEAD_ZONE = 0x10,
  PARAM_MOTOR_ROUND_UP = 0x11,
  PARAM_STEER_AMOUNT = 0x12,
  PARAM_M1_BOOST = 0x13,
  PARAM_M2_BOOST = 0x14,
  /* 0x20-0x2F -- float */
  PARAM_KP = 0x20,
  PARAM_KD = 0x21,
  PARAM_KI = 0x22,
  PARAM_TARGET_ANGLE = 0x23,
  PARAM_HIGHPASS_COEFF = 0x24,
  PARAM_LOWPASS_COEFF = 0x25,
  PARAM_MAX_ERROR_INT = 0x26,
  PARAM_ACCEL_OFFSET = 0x27,
};

float* getFloatParamPointer(uint8_t code) {
  switch (code) {
  case PARAM_KP:
    return &Kp;
  case PARAM_KD:
    return &Kd;
  case PARAM_KI:
    return &Ki;
  case PARAM_TARGET_ANGLE:
    return &targetAngle;
  case PARAM_HIGHPASS_COEFF:
    return &highpassCoeff;
  case PARAM_LOWPASS_COEFF:
    return &lowpassCoeff;
  case PARAM_MAX_ERROR_INT:
    return &maxErrorInt;
  case PARAM_ACCEL_OFFSET:
    return &accelOffset;
  }
  return NULL;
}

int16_t* getIntParamPointer(uint8_t code) {
  switch (code) {
    case PARAM_MOTOR_DEAD_ZONE:
      return &motorDeadZone;
    case PARAM_MOTOR_ROUND_UP:
      return &motorRoundUp;
    case PARAM_STEER_AMOUNT:
      return &steerAmount;
    case PARAM_M1_BOOST:
      return &m1Boost;
    case PARAM_M2_BOOST:
      return &m2Boost;
  }
  return NULL;
}

enum TelemetryItem {
  /* 0x60-0x6F -- uint16 reports */
  ITEM_M1_CURRENT_MA = 0x60,
  ITEM_M2_CURRENT_MA = 0x61,
  /* 0x70-0x7F -- int16 reports */
  ITEM_M1_COMMAND = 0x70,
  ITEM_M2_COMMAND = 0x71,
  ITEM_PID_OUTPUT = 0x72,
  /* 0x80-0x8F -- float reports */
  ITEM_ACCEL_THETA_DEG = 0x80,
  ITEM_GYRO_THETADOT_DPS = 0x81,
  ITEM_EST_THETA_DEG = 0x82,
  ITEM_EST_ERROR_DEG = 0x83,
  ITEM_ERROR_INT_DS = 0x84,
  ITEM_BATTERY_VOLTS = 0x85
};

void setupTelemetry() {
  Comm.onParameterRequest = onParameterRequest;
  Comm.onSetParameter = onSetParameter;
  Comm.onTelemetryRequest = onTelemetryRequest;

  // These items before first group are by request only
  Telemetry.registerDataItem(getBatteryVoltage, ITEM_BATTERY_VOLTS);
  Telemetry.registerDataItem(getM1Current, ITEM_M1_CURRENT_MA);
  Telemetry.registerDataItem(getM2Current, ITEM_M2_CURRENT_MA);

  Telemetry.beginFirstGroup();
  Telemetry.registerDataItem(output, ITEM_PID_OUTPUT);
  Telemetry.registerDataItem(outputM1, ITEM_M1_COMMAND);
  Telemetry.registerDataItem(outputM2, ITEM_M2_COMMAND);

  // These four items need to be together for the pid component graph
  Telemetry.beginGroup();
  Telemetry.registerDataItem(estError, ITEM_EST_ERROR_DEG);
  Telemetry.registerDataItem(thetaDotG, ITEM_GYRO_THETADOT_DPS);
  Telemetry.registerDataItem(errorInt, ITEM_ERROR_INT_DS);
  Telemetry.registerDataItem(output, ITEM_PID_OUTPUT);

  // The first is required for the angle graph, the second isn't anymore
  Telemetry.beginGroup();
  Telemetry.registerDataItem(estTheta, ITEM_EST_THETA_DEG);
  Telemetry.registerDataItem(thetaA, ITEM_ACCEL_THETA_DEG);
}

void onParameterRequest(uint8_t code) {
  uint8_t *p;
  uint8_t buffer[5];
  buffer[0] = code;
  if (code < MSG_REQUEST_PARAM + MSG_FLOAT) {
    p = (uint8_t*)getIntParamPointer(code);
    if (p == NULL) return;
    buffer[1] = p[1];
    buffer[2] = p[0];
    Comm.write(buffer, 3, false);
  }
  else {
    p = (uint8_t*)getFloatParamPointer(code);
    if (p == NULL) return;
    buffer[1] = p[3];
    buffer[2] = p[2];
    buffer[3] = p[1];
    buffer[4] = p[0];
    Comm.write(buffer, 5, false);
  }
}

void onSetParameter(uint8_t code, const uint8_t *data, size_t length) {
  uint8_t *param;
  if (length == 4) {
    param = (uint8_t*)getFloatParamPointer(code);
    if (param == NULL) return;
    param[0] = data[3];
    param[1] = data[2];
    param[2] = data[1];
    param[3] = data[0];
  }
  if (length == 2) {
    param = (uint8_t*)getIntParamPointer(code);
    if (param == NULL) return;
    param[0] = data[1];
    param[1] = data[0];
  }
}

void onTelemetryRequest(uint8_t code) {
  Telemetry.writeItem(code);
}

float getBatteryVoltage() {
  return (analogRead(batterySensePin)+21.6972) / 74.9734;
}

unsigned int getM1Current() {
  return Motors.getM1CurrentMilliamps();
}

unsigned int getM2Current() {
  return Motors.getM2CurrentMilliamps();
}
