/**
 * Motor support for main control program
 **/

#include "Motors.h"

SegwayMotors::SegwayMotors() :
md() {
  stopFlag = false;
}

void SegwayMotors::begin() {
  md.init();
  md.setSpeeds(0, 0);
}

void SegwayMotors::setSpeeds(int m1Speed, int m2Speed) {
  if (!stopFlag) {
    md.setSpeeds(m1Speed, m2Speed);
  }
}

void SegwayMotors::setM1Speed(int speed) {
  if (!stopFlag) {
    md.setM1Speed(speed);
  }
}

void SegwayMotors::setM2Speed(int speed) {
  if (!stopFlag) {
    md.setM2Speed(speed);
  }
}

void SegwayMotors::setBrakes(int m1Brake, int m2Brake) {
  md.setBrakes(m1Brake, m2Brake);
}

void SegwayMotors::setM1Brake(int brake) {
  md.setM1Brake(brake);
}

void SegwayMotors::setM2Brake(int brake) {
  md.setM2Brake(brake);
}

void SegwayMotors::kill() {
  stopFlag = true;
  setBrakes(400, 400);
}

void SegwayMotors::faultCheck() {
  if (md.getM1Fault() || md.getM2Fault()) {
    kill();
  }
}

unsigned int SegwayMotors::getM1CurrentMilliamps() {
  return md.getM1CurrentMilliamps();
}

unsigned int SegwayMotors::getM2CurrentMilliamps() {
  return md.getM2CurrentMilliamps();
}

SegwayMotors Motors = SegwayMotors();
