/********************************************************
 * Main Control for the big boy Segway
 ********************************************************/

#include <PID_v1.h>
#include <Wire.h>
#include <Segway_ADXL345.h>
#include <L3G.h>
#include "DualVNH5019MotorShield.h"

#define BAUD 115200

// Define Variables we'll be connecting to
double Setpoint, Input, Output;
Segway_ADXL345 accel;
L3G gyro;

DualVNH5019MotorShield md;

const int potPin = A2;
const int stopButton = 3;
const int brakeButton = 5;

double Kp=2, Ki=0, Kd=0; // Kd produces a lot of jitter
bool toggleFlag = false;

float gyro_scale = 70;
// initialize LUT
float arcsin_table[256];

// Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);


void setup() {
  // Setup the accelerometer
  Wire.begin();
  Serial.begin(BAUD);
  if (accel.begin()) {
    Serial.println("Found AXDL345 Accelerometer");
  }
  else {
    Serial.println("Can't connect  to accelerometer");
    delay(5000);
  }
  if (!gyro.init()) {
    Serial.println("Failed to autodetect gyro type!");
    while (1);
  }

  gyro.enableDefault();

  delay(150);
  accel.setRange(accel.ADXL345_RANGE_4_G);
  accel.setDataRate(accel.ADXL345_DATARATE_800_HZ);
  accel.setXOffset(1.1);
  accel.setYOffset(0.3);
  accel.setZOffset(1.7);

  // initialize the variables we're linked to
  accel.poll();
  Input = accel.getZ();
  Setpoint = 0;

  // turn on PID
  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(-511, 511);
  myPID.SetSampleTime(1);

  md.init();
  pinMode(stopButton, INPUT_PULLUP);
  pinMode(brakeButton, INPUT_PULLUP);
  md.setSpeeds(0, 0);


  // calculate LUT in degrees
  for(int i=0;i < 256; i++) {
    arcsin_table[i] = asin((float(i)-128.0)/128.0)*180/PI;
  }
}

void stopIfFault() {
  if (md.getM1Fault()) {
    Serial.println("M1 fault");
    while(1);
  }
  if (md.getM2Fault()) {
    Serial.println("M2 fault");
    while(1);
  }
}

void loop() {
  toggleFlag ? (digitalWrite(13, HIGH)) : (digitalWrite(13, LOW));
  toggleFlag = !toggleFlag;
  // approx 720 Hz

  // read from sensors
  accel.poll();
  gyro.read();

  // filter sensor data to Input
  float z = accel.getZ();
  float x = accel.getX();

  float tilt_scaled= -z/9.81;//forward fall is positive angle
  //float tilt_scaled= -z/sqrt(pow(x,2) + pow(z,2));//forward fall is positive angle
  int index = (tilt_scaled+1)*127+1;
  float theta = arcsin_table[constrain(index,0,255)];




  Input = 0.97 * (Input + ((float)gyro.g.y / gyro_scale) * 0.00138);
  //Input = 0.97 * (Input + (float)gyro.g.y * 0.00138);
  Input += 0.03 * ((float)theta);

  myPID.SetTunings(Kp, Ki, Kd);
  myPID.Compute();


  // Brake, coast, or apply new speed
  if (digitalRead(brakeButton) == LOW) {
    md.setBrakes(400, 400);
  }
  else if (digitalRead(stopButton) == LOW) {
    Kp = analogRead(potPin) / 4.0;
  }
  else {
    md.setSpeeds(Output, Output);
  }

  stopIfFault();
}
