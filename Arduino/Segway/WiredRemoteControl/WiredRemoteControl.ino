/**
 * Enables manual control of the motors using the wired remote. Note that the
 * low speed region is cut out and the speed jumps from -min to +min at the
 * center point of the speed control.
 *
 * This design isn't fail-safe: motors are not off when the remote disconncted!
 * 
 * Speed potentiometer - connect across Vcc and GND with wiper on analog input.
 *   zero point = max backward, just below center = min forward, just above
 *   center = min foward, max value = max foward
 * Coast and brake buttons - connect across digital pins and ground. External
 *   pull ups are not required.
 **/

#include "DualVNH5019MotorShield.h"

DualVNH5019MotorShield md;

const int potPin = A2;
const int stopButton = 3;
const int brakeButton = 5;

const int minSpeed = 25;  // both 0 to 400
const int maxSpeed = 200;

void setup() {
  Serial.begin(115200);
  md.init();
  pinMode(stopButton, INPUT_PULLUP);
  pinMode(brakeButton, INPUT_PULLUP);
}

void loop() {
  // Read raw speed input
  // maps 0-511 -> 511-0 (flipped) and 512-1023 -> 0-511
  int rawSpeed = analogRead(potPin);
  int absUnscaledSpeed = (rawSpeed < 512) ? (511 - rawSpeed): (rawSpeed - 512);

  // Map and scale to user defined range
  int newSpeed;
  if (rawSpeed < 512) {
    newSpeed = map(absUnscaledSpeed, 0, 511, -minSpeed, -maxSpeed);
  } 
  else {
    newSpeed = map(absUnscaledSpeed, 0, 511, minSpeed, maxSpeed);
  }

  // Brake, coast, or apply new speed
  if (digitalRead(brakeButton) == LOW) {
    md.setBrakes(400, 400);
  } 
  else if (digitalRead(stopButton) == LOW) {
    md.setBrakes(0, 0);
  } 
  else {
    md.setSpeeds(newSpeed, newSpeed);
  }

  stopIfFault();
  delay(10);
}

void stopIfFault() {
  if (md.getM1Fault()) {
    Serial.println("M1 fault");
    while(1);
  }
  if (md.getM2Fault()) {
    Serial.println("M2 fault");
    while(1);
  }
}


