#include <AFMotor.h>

AF_DCMotor motor1(1);
AF_DCMotor motor2(2);

void setup() {
  motor1.setSpeed(1000);
  motor2.setSpeed(1000);

  motor1.run(RELEASE);
  motor2.run(RELEASE);
}

void loop() {
  motor1.run(FORWARD);
  motor2.run(FORWARD);
  delay(200);

  motor1.run(RELEASE);
  motor2.run(RELEASE);
  delay(200);

  motor1.run(BACKWARD);
  motor2.run(BACKWARD);
  delay(200);

  motor1.run(RELEASE);
  motor2.run(RELEASE);
  delay(200);
}

