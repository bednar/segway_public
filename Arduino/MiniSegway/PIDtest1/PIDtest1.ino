/********************************************************
 * PID Basic Example
 * Reading analog input 0 to control analog PWM output 3
 ********************************************************/

#include <PID_v1.h>
#include <Wire.h>
#include <Segway_ADXL345.h>
#include <AFMotor.h>

Segway_ADXL345 accel;
AF_DCMotor motor1(1);
AF_DCMotor motor2(2);

// Define Variables we'll be connecting to
double Setpoint, Input, Output;

// Define the aggressive and conservative Tuning Parameters
double aggKp=100, aggKi=0.1, aggKd=10;
double consKp=5, consKi=0.05, consKd=2;

// Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint, consKp, consKi, consKd, DIRECT);

void setup()
{
  // Setup the accelerometer
  Wire.begin();
  Serial.begin(57600);
  if (accel.begin()) {
    Serial.println("Found AXDL345 Accelerometer");
  } 
  else {
    Serial.println("Can't connect  to accelerometer");
    delay(5000);
  }
  delay(150);
  accel.setRange(accel.ADXL345_RANGE_4_G);
  accel.setDataRate(accel.ADXL345_DATARATE_50_HZ);
  accel.setXOffset(1.1);
  accel.setYOffset(0.3);
  accel.setZOffset(1.7);

  //initialize the variables we're linked to
  //Input = analogRead(0);
  accel.poll();
  Input = accel.getZ();
  Setpoint = 0;

  //turn the PID on
  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(-100, 100);

  // turn on motor
  motor1.setSpeed(200);
  motor2.setSpeed(200);

  motor1.run(RELEASE);
  motor2.run(RELEASE);
}

void loop()
{

  accel.poll();
  Input = accel.getZ();

  double gap = abs(Setpoint-Input); //distance away from setpoint
  if(gap<1.5) {
    //we're close to setpoint, use conservative tuning parameters
    myPID.SetTunings(consKp, consKi, consKd);
  }
  else {
    //we're far from setpoint, use aggressive tuning parameters
    myPID.SetTunings(aggKp, aggKi, aggKd);
  }

  myPID.Compute();
  Serial.println(Output);

  if (Output > 0) {
    motor1.setSpeed(Output * 2);
    motor2.setSpeed(Output * 2);
    motor1.run(FORWARD);
    motor2.run(FORWARD);
  }
  else {
    motor1.setSpeed(abs(Output * 2));
    motor2.setSpeed(abs(Output * 2));
    motor1.run(BACKWARD);
    motor2.run(BACKWARD);
  }
}

