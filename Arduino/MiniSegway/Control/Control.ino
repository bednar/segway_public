/********************************************************
 * Main Control for mini-Segway
 ********************************************************/

#include <PID_v1.h>
#include <Wire.h>
#include <Segway_ADXL345.h>
#include <AFMotor.h>
#include <L3G.h>

#define BAUD 115200

// Define Variables we'll be connecting to
double Setpoint, Input, Output;
Segway_ADXL345 accel;
L3G gyro;
AF_DCMotor motor1(1);
AF_DCMotor motor2(2);

// Define the aggressive and conservative Tuning Parameters
// double aggKp=100, aggKi=0.1, aggKd=10;
// double consKp=5, consKi=0.05, consKd=2;
double Kp=350, Ki=0, Kd=0; // Kd produces a lot of jitter
bool toggleFlag = false;

double gyro_scale = 70;
// initialize LUT
double arcsin_table[256];

// Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

// Serial stuff
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
char cmdbuffer[50];

void setup() {
  inputString.reserve(200);
  // Setup the accelerometer
  Wire.begin();
  Serial.begin(BAUD);
  if (accel.begin()) {
    Serial.println("Found AXDL345 Accelerometer");
  } 
  else {
    Serial.println("Can't connect  to accelerometer");
    delay(5000);
  }
  if (!gyro.init()) {
    Serial.println("Failed to autodetect gyro type!");
    while (1);
  }

  gyro.enableDefault();

  delay(150);
  accel.setRange(accel.ADXL345_RANGE_4_G);
  accel.setDataRate(accel.ADXL345_DATARATE_50_HZ);
  accel.setXOffset(1.1);
  accel.setYOffset(0.3);
  accel.setZOffset(1.7);

  // initialize the variables we're linked to
  accel.poll();
  Input = accel.getZ();
  Setpoint = 0;

  // turn on PID
  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(-255, 255);
  myPID.SetSampleTime(1);

  // turn on motor
  motor1.setSpeed(0);
  motor2.setSpeed(0);
  motor1.run(RELEASE);
  motor2.run(RELEASE);

  // calculate LUT in degrees
  for(int i=0;i < 256; i++) {
    arcsin_table[i] = asin((double(i)-128.0)/128.0)*180/PI;
  } 
}

void printTunings(double newKp, double newKi, double newKd) {
  //Serial.println("Ki = " + Ki, + ", " + "Kp = "+ Kp + ", " + "Kd = " + Kd);
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}

void loop() {

  // toggleFlag ? (digitalWrite(7, HIGH)) : (digitalWrite(7, LOW));
  // toggleFlag = !toggleFlag;
  // approx 720 Hz

  // print the string when a newline arrives:
  if (stringComplete) {
    inputString.trim();
    String cmd = inputString.substring(0, 2);
    if(cmd.equals("kp")) {
      (inputString.substring(2)).toCharArray(cmdbuffer, 50);
      Kp = atof(cmdbuffer);
      // printTunings();
    }
    else if(cmd.equals("ki")) {
      (inputString.substring(2)).toCharArray(cmdbuffer, 50);
      Ki = atof(cmdbuffer);
      // printTunings();
    }
    else if(cmd.equals("kd")) {
      (inputString.substring(2)).toCharArray(cmdbuffer, 50);
      Kd = atof(cmdbuffer);
      // printTunings();
    }
    Serial.println(inputString); 
    // clear the string:
    inputString = "";
    stringComplete = false;
  }

  // read from sensors
  accel.poll();
  gyro.read();

  // filter sensor data to Input
  double z = accel.getZ();
  double x = accel.getX();

  double tilt_scaled= -z/sqrt(pow(x,2) + pow(z,2));//forward fall is positive angle
  int index = (tilt_scaled+1)*127+1;
  double theta = arcsin_table[constrain(index,0,255)];

  Input = 0.97 * (Input + ((float)gyro.g.y / gyro_scale) * 0.00138);
  // Input = 0.97 * (Input + (float)gyro.g.y * 0.00138);
  Input += 0.03 * ((float)accel.getZ());

  // Serial.print("gyro y: ");
  // Serial.println((float)gyro.g.y);
  // Serial.print("accel theta: ");
  // Serial.println(theta);
  // Serial.println(Input);

  /*
  // determine whether to use aggressive gains
  double gap = abs(Setpoint-Input); //distance away from setpoint
  if (gap<1.5) {  //we're close to setpoint, use conservative tuning parameters
    myPID.SetTunings(consKp, consKi, consKd);
  }
  else {
    //we're far from setpoint, use aggressive tuning parameters
    myPID.SetTunings(aggKp, aggKi, aggKd);
  }
  */
  myPID.SetTunings(Kp, Ki, Kd);

  myPID.Compute();
  // Serial.println(Output);

  if (Output > 0) {
    motor1.setSpeed(Output);
    motor2.setSpeed(Output);
    motor1.run(FORWARD);
    motor2.run(FORWARD);
  }
  else {
    motor1.setSpeed(abs(Output));
    motor2.setSpeed(abs(Output));
    motor1.run(BACKWARD);
    motor2.run(BACKWARD);
  }
}

