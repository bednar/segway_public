// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>

AF_DCMotor motor(1);
AF_DCMotor motor2(2);

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test! ");

  // turn on motor
  motor.setSpeed(1000);
  motor2.setSpeed(1000);
 
  motor.run(RELEASE);
  motor2.run(RELEASE);
}

void loop() {
  uint8_t i;
  
  Serial.print("tick ");
  
  
  motor.run(FORWARD);
  motor2.run(FORWARD);
  for (i=127; i<255; i++) {
    motor.setSpeed(1);  
    motor2.setSpeed(i);  
    delay(10);
  }
 
 
 
  for (i=255; i!=127; i--) {
    motor.setSpeed(i);  
    motor2.setSpeed(i);  
    delay(10);
 }
 
  
  Serial.print("tock ");


  motor.run(BACKWARD);
  motor2.run(BACKWARD);
  for (i=127; i<255; i++) {
    motor.setSpeed(i);  
    motor2.setSpeed(i);  
    delay(10);
 }
 
 
 
  for (i=255; i!=127; i--) {
    motor.setSpeed(i);  
    motor2.setSpeed(i);  
    delay(10);
 }
 
  

  Serial.print("tech \n");
  motor.run(RELEASE);
  motor2.run(RELEASE);
  delay(1000);
}
